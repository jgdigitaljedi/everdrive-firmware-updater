# Other links

While working on this tool, I attempted to add as many update checks as possible for as many modern retro gaming items as possible. I found that lots of devices have sign in requirements to download firmware updates, are self updating using wifi, host their downloads on file sharing services which are unreliable to scrape, don't version their files, etc. That said, it didn't make sense to add them to this tool or simply wasn't possible so I decided to add a list of links here for those items for anyone that would like to explore additional the additional items or would like a single location for those update links.

<table>
  <thead>
    <tr>
      <td>Item</td>
      <td>Type</td>
      <td>Platform</td>
      <td>Link</td>
      <td>Notes</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Bennvenn El Cheapo</td>
      <td>flashcart</td>
      <td>Atari Lynx</td>
      <td>
        <a href="https://bennvenn.myshopify.com/pages/downloads">Link</a>
      </td>
      <td>files not versioned on site</td>
    </tr>
    <tr>
      <td>NeoSD</td>
      <td>flashcart</td>
      <td>Neo Geo AES</td>
      <td>
        <a href="https://terraonion.com/en/producto/neosd-aes/">Link</a>
      </td>
      <td>firmware downloads require account and login to access</td>
    </tr>
    <tr>
      <td>NES PowerPak</td>
      <td>flashcart</td>
      <td>NES</td>
      <td>
        <a href="https://www.retrousb.com/product_info.php?products_id=34">Link</a>
      </td>
      <td>discontinued; no more updates</td>
    </tr>
    <tr>
      <td>Super SD System 3</td>
      <td>flashcart</td>
      <td>PC Engine/TurboGrafx-16</td>
      <td>
        <a href="https://terraonion.com/en/producto/super-sd-system-3/">Link</a>
      </td>
      <td>firmware downloads require account and login to access</td>
    </tr>
    <tr>
      <td>Jaguar GameDrive</td>
      <td>flashcart</td>
      <td>Atari Jaguar</td>
      <td>
        <a href="https://www.retrohq.co.uk/products/atari-jaguar-gd-flash-cartridge">Link</a>
      </td>
      <td>website says to insert your GameDrive and follow the link on your screen to get firmware</td>
    </tr>
    <tr>
      <td>Colecovision AtariMax Ultimate SD</td>
      <td>flashcart</td>
      <td>Colecovision</td>
      <td>
        <a href="https://www.atarimax.com/colecosd/documentation/">Link</a>
      </td>
      <td>website says to email creator for firmware link</td>
    </tr>
    <tr>
      <td>Atari 5200 AtariMax Ultimate SD</td>
      <td>flashcart</td>
      <td>Atari 5200</td>
      <td>
        <a href="https://www.atarimax.com/5200sd/documentation/">Link</a>
      </td>
      <td>website says to email creator for firmware link</td>
    </tr>
    <tr>
      <td>R4 SDHC</td>
      <td>flashcart</td>
      <td>Nintendo DS/DSi/3DS</td>
      <td>
        <a href="https://www.r43ds.org/pages/R4-3DS-Dual-Core-Firmware.html">Link</a>
      </td>
      <td>download links were dead at time of trying to add this device</td>
    </tr>
    <tr>
      <td>PSIO</td>
      <td>optical drive emulator</td>
      <td>PlayStation</td>
      <td>
        <a href="https://ps-io.com/downloads/">Link</a>
      </td>
      <td>website requires email address, order number, and serial number to get firmware downloads</td>
    </tr>
    <tr>
      <td>MODE</td>
      <td>optical drive emulator</td>
      <td>Sega Dreamcast/Sega Saturn</td>
      <td>
        <a href="https://terraonion.com/en/producto/terraonion-mode/">Link</a>
      </td>
      <td>firmware downloads require account and login to access</td>
    </tr>
    <tr>
      <td>GDEMU</td>
      <td>optical drive emulator</td>
      <td>Sega Dreamcast</td>
      <td>
        <a href="https://gdemu.wordpress.com/links/">Link</a>
      </td>
      <td>firmware download provided by file sharing service</td>
    </tr>
  </tbody>
</table>

## More sources

I obviously had to source the information regarding what devices, software, etc exist and find links for them. If you would like to dig deeper yourself, checkout these sites as they are filled with great information.

- (Consolemods Wiki)[https://consolemods.org/wiki/Main_Page]
- (RetroRGB)[https://www.retrorgb.com/romcarts.html]
