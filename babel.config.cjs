module.exports = {
  presets: [['@babel/preset-env', { targets: { node: 'current' }, modules: 'auto' }]],
  sourceType: 'module',
  plugins: ['@babel/plugin-transform-runtime', '@babel/plugin-syntax-import-assertions']
};
