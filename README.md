# Retro Gamer Firmware Updater

## What it does

The general idea here is to have a script that can be run from the command line that checks for forware/software updates for various retro gaming devices (EverDrive, optical drive emulators, alternative operating systems for devices, hacking software, etc). If an updated firmware/software version is found for one of you devices, the file for the update will be automatically downloaded so all you have to do is extract (if compressed) it and copy it to your device's storage (SD/micro SD/etc). Obviously, if a device or app has an online updater already (like a MiSTer, EmuDeck, Pocket Sync, Steam Deck, etc) then there is no need for this tool to check for those updates. I set out to have a large list of supported devices, but I encountered limitations that prevented many devices from being added (like requiring an account be logged in to see the firware updates, updated files not having any sort of versioning on the page to tell if it is new, etc). That said, this is useful for many of the EverDrives, a small handful of ODEs, some retro gaming related software, and a few other things.

As of right now, there are direct firmware checks for 22 flashcarts (18 EverDrives/4 other), software update checks for 22 applications (some firmware flashers, ODE loader software, homebrew apps, etc), firmware/software checks for 8 devices, software/firmware checks for 5 optical drive emulators, and firmware cheks for 2 mods. That's a total of 58 update checks that can be done right now!

---

## General setup

The `config.json` file in the root of the project has to be setup with your devices and current firware version. If you don't know the current version of your firmware for each device, you can sometimes check for a version number in the directory of the firmware on your device's storage. If you can't find your current firmware version, you can always pass a blank string on the first run, download the firmware, and update your `config.json` file with the versions logged to the console. If you have questions about how to setup your config, you can checkout the config files in the `testConfigs` directory. The `allConfig.json` file has a config entry for every supported device and is used by me to test my scripts and make sure they work so this is probably the best example as it shows the firmware version format for each device and usually has the latest version of each device firmware in it. The `myConfig.json` is what I use for my devices and is usually what I restore the `config.json` file to before I push new changes.

To get the script ready, clone it to a directory of your choosing, navigate into that directory, and run `npm install` to install the dependencies. _Note this this tool has a dependency of Node version 16 or higher to work!_ If you do not know what Node JS is, this tool isn't for you. I have plans to eventually make this into a desktop app that will be easily usable by anyone and combine the logic here with my Analogue Pocket update notifier scripts to make a one experience that can help you manage updates for all of your retro gaming devices.

---

## Running

Once this script is cloned and the npm dependencies are installed, you can simply run `npm start` to run the script. Conversely, you can alias the script like I do and run it with a single command from anywhere. Any updates will log to the console and be downloaded to the `downloads` folder in the corresponding sub directory. Also, to make it simpler to update your config file, you can find a copy of your config with all of the latest versions of everything in `data/updatedConfig.json` after this script is run. If you updated all of your items after running this tool, you can simply copy the contents of that file to `config.json` in the root to instantly update your config.

---

## Disclaimer

For each device you add to this little script, the firmware versions and download links are obtained via web scraping. That said, if a firmware site changes their site's structure, it can break this functionality. This is one of the downsides to web scraping, but that's really the only way this can be done as there is not a centralized database or anything from which to check for firmware updates. I use this myself, albeit for a few EverDrives and my GCLoader so if something changes on those sites, I'll likely fix the scraping logic very quickly. Otherwise, I'll try to set the config to check every device periodically to make sure that the scraping is still working for all devices.

Also, I'm doing a lot of web scraping here, but some devices/software host updates on GitHub which isn't easily scrapeable and requires use of the GitHub API. That said, _if you are using this to check for updates for the devices or software, you will need a GitHub API key to use this tool!_

- XStation
- Swiss
- TEOS
- Harmony Cart
- XPort
- TonyHax
- Open PS2 Loader
- PSX Pi SmbShare
- GB Interceptor
- Picoboot
- GameCube Memory Manager
- Free PSX Boot
- Unirom
- Hackchi2
- Tinkup

If you are in that boat and don't have a GitHub API key (they call it a personal access token), you can read more about that [HERE](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token). Once you have you key, you need to set it to your environment variable as "GITHUB_KEY" to be pulled into the script. It is free of charge to get a personal access token from GitHub.

ALSO, I have now encountered the situation where a firmware site let's the SSL certificate expire. At first glance, it appeared as if I could simply change the URL to http and this would work, but I realized the issue is that the site in question did not update their firmware links, which makes sense, but this means that additional logic would be needed to remove the s in https that comes back from the site scraping. At the moment, I'm thinking that the best way to handle this is to simply wait a while and see if the site renews their SSL cert because this m would make the original logic work again and I think is the likely outcome. If a site allows their cert to expire for long enough, I will adjust the code for that firmware check assuming the site is not being maintained well.

---

## Devices supported

This script was JUST created so the device list is small, but constantly growing. The following devices are currently supported:

### Flash carts

<table>
  <thead>
    <tr>
      <td>
        <strong>Flash cart</strong>
      </td>
      <td>
        <strong>Version(s)</strong>
      </td>
      <td>
        <strong>Config file key</strong>
      </td>
      <td>
        <strong>Link</strong>
      </td>
    </tr>
  <thead>
  <tbody>
    <tr>
      <td>
        EverDrive Mega X (Genesis/Mega Drive)
      </td>
      <td>
        v2/x3/x5/x7
      </td>
      <td>
        megaX
      </td>
      <td>
        <a href="https://stoneagegamer.com/mega-everdrive-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        EverDrive Mega Pro (Genesis/Mega Drive)
      </td>
      <td>
        Pro
      </td>
      <td>
        megaPro
      </td>
      <td>
        <a href="https://stoneagegamer.com/mega-everdrive-pro-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Mega EverDrive (legacy) (Genesis/Mega Drive)
      </td>
      <td>
        v1
      </td>
      <td>
        megaV1
      </td>
      <td>
        <a href="https://stoneagegamer.com/legacy-everdrive-flash-cart-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Super EverDrive (legacy) (SNES)
      </td>
      <td>
        v1/v2
      </td>
      <td>
        superEd
      </td>
      <td>
        <a href="https://stoneagegamer.com/legacy-everdrive-flash-cart-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        FXPAK / SD2SNES (SNES)
      </td>
      <td>
        all
      </td>
      <td>
        fxPak
      </td>
      <td>
        <a href="https://stoneagegamer.com/fxpak-sd2snes-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        EverDrive64 (N64)
      </td>
      <td>
        v2.5x/v3.x/x5/x7
      </td>
      <td>
        ed64
      </td>
      <td>
        <a href="https://stoneagegamer.com/everdrive64-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        EverDrive64 (legacy) (N64)
      </td>
      <td>
        v1.x
      </td>
      <td>
        ed64V1
      </td>
      <td>
        <a href="https://stoneagegamer.com/legacy-everdrive-flash-cart-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        EverDrive-GB (Game Boy)
      </td>
      <td>
        x3/x5/x7
      </td>
      <td>
        edGb
      </td>
      <td>
        <a href="https://stoneagegamer.com/everdrive-gb-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        EverDrive-GB (legacy) (Game Boy)
      </td>
      <td>
        v1.x
      </td>
      <td>
        edGbV1
      </td>
      <td>
        <a href="https://stoneagegamer.com/legacy-everdrive-flash-cart-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        EverDrive-GBA (Game Boy Advance)
      </td>
      <td>
        X5
      </td>
      <td>
        gbaEd
      </td>
      <td>
        <a href="https://stoneagegamer.com/everdrive-gba-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Turbo EverDrive (TurboGrafx-16/PC Engine)
      </td>
      <td>
        v2
      </td>
      <td>
        turboEd
      </td>
      <td>
        <a href="https://stoneagegamer.com/turbo-everdrive-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Turbo EverDrive (legacy) (TurboGrafx-16/PC Engine)
      </td>
      <td>
        v1
      </td>
      <td>
        turboEdV1
      </td>
      <td>
        <a href="https://stoneagegamer.com/legacy-everdrive-flash-cart-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        EverDrive-N8 (NES)
      </td>
      <td>
        original (non-Pro)
      </td>
      <td>
        n8
      </td>
      <td>
        <a href="https://stoneagegamer.com/everdrive-n8-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        EverDrive-N8 Pro (NES)
      </td>
      <td>
        Pro
      </td>
      <td>
        n8Pro
      </td>
      <td>
        <a href="https://stoneagegamer.com/everdrive-n8-pro-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Master EverDrive (Sega Master System)
      </td>
      <td>
        x7
      </td>
      <td>
        masterEd
      </td>
      <td>
        <a href="https://stoneagegamer.com/master-everdrive-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Master EverDrive (legacy) (Sega Master System)
      </td>
      <td>
        v1
      </td>
      <td>
        masterEdV1
      </td>
      <td>
        <a href="https://stoneagegamer.com/legacy-everdrive-flash-cart-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        EverDrive-GG (Game Gear)
      </td>
      <td>
        x7
      </td>
      <td>
        edGg
      </td>
      <td>
        <a href="https://stoneagegamer.com/everdrive-gg-downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        64Drive (N64)
      </td>
      <td>
        hw1 / hw2
      </td>
      <td>
        hw1_drive64 / hw2_drive64
      </td>
      <td>
        <a href="http://64drive.retroactive.be/support.php">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Neo Pocket GameDrive (Neo Geo Pocket/Color)
      </td>
      <td>
        all
      </td>
      <td>
        neoPocketGD
      </td>
      <td>
        <a href="https://www.retrohq.co.uk/products/neo-geo-pocket-neopocket-gamedrive-flash-cartridge">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        TEOS alt firmware for TurboEverDrive v2 (Turbografx-16/PC Engine)
      </td>
      <td>
        v2
      </td>
      <td>
        teos
      </td>
      <td>
        <a href="https://github.com/jbrandwood/teos/releases">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Turbo EverDrive Pro (Turbografx-16/PC Engine)
      </td>
      <td>
        original
      </td>
      <td>
        turboEdPro
      </td>
      <td>
        <a href="https://krikzz.com/pub/support/turbo-everdrive/pro-series/firmware/">Link</a>
      </td>
    </tr>
  </tbody>
</table>

### Optical Drive Emulators (ODE)

<table>
  <thead>
    <tr>
      <td>
        <strong>ODE</strong>
      </td>
      <td>
        <strong>Version(s)</strong>
      </td>
      <td>
        <strong>Config file key</strong>
      </td>
      <td>
        <strong>Link</strong>
      </td>
    </tr>
  <thead>
  <tbody>
    <tr>
      <td>
        GCLoader (GameCube)
      </td>
      <td>
        original
      </td>
      <td>
        gcloader
      </td>
      <td>
        <a href="https://gc-loader.com/firmware-updates/">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Fenrir (Sega Saturn)
      </td>
      <td>
        original
      </td>
      <td>
        fenrir
      </td>
      <td>
        <a href="https://www.fenrir-ode.fr/firmware/#version-history">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Satiator (Sega Saturn)
      </td>
      <td>
        original
      </td>
      <td>
        satiator
      </td>
      <td>
        <a href="https://info.satiator.net/firmware/">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        XStation (PlayStation 1)
      </td>
      <td>
        original
      </td>
      <td>
        xstation
      </td>
      <td>
        <a href="https://github.com/x-station/xstation-releases/releases">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        XPort (REAL 3DO)
      </td>
      <td>
        original
      </td>
      <td>
        xport
      </td>
      <td>
        <a href="https://github.com/fixelsan/3do-ode-firmware/releases">Link</a>
      </td>
    </tr>
  </tbody>
</table>

### Software

<table>
  <thead>
    <tr>
      <td>
        <strong>Software</strong>
      </td>
      <td>
        <strong>Config file key</strong>
      </td>
      <td>
        <strong>Link</strong>
      </td>
    </tr>
  <thead>
  <tbody>
    <tr>
      <td>
        Swiss (GameCube)
      </td>
      <td>
        swiss
      </td>
      <td>
        <a href="https://github.com/emukidid/swiss-gc/releases">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Free McBoot (PlayStation 2)
      </td>
      <td>
        freemcboot
      </td>
      <td>
        <a href="https://israpps.github.io/FreeMcBoot-Installer/test/8_Downloads.html">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        FlashMasta updater software
      </td>
      <td>
        flashmasta_mac/flashmasta_linux/flashmasta_windows
      </td>
      <td>
        <a href="https://www.flashmasta.com/software-downloads/">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        LTO Flash user interface software (Intellivision)
      </td>
      <td>
        ltoflash_mac/ltoflash_source/ltoflash_windows
      </td>
      <td>
        <a href="http://www.intvfunhouse.com/intvfunhouse/ltoflash/">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Harmony Cart software (Atari 2600)
      </td>
      <td>
        harmonycart_mac/harmonycart_source/harmonycart_windows/harmonycart_linux
      </td>
      <td>
        <a href="https://github.com/sa666666/harmonycart/releases">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        TonyHax software (PlayStation)
      </td>
      <td>
        tonyhax
      </td>
      <td>
        <a href="https://github.com/socram8888/tonyhax/releases">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Open PS2 Loader (PlayStation 2)
      </td>
      <td>
        opl
      </td>
      <td>
        <a href="https://github.com/ps2homebrew/Open-PS2-Loader/releases/tag/v1.0.0">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        RetroUSB Scoreboard (firmware checks done with 'retroUsbAvs' in 'devices' section)
      </td>
      <td>
        retroUsbSb_mac/retroUsbSb_win
      </td>
      <td>
        <a href="https://www.retrousb.com/product_info.php?cPath=36&products_id=78&osCsid=2945a80f5930658692da052e1cfd53fd">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Raphnet Adapter Manager
      </td>
      <td>
        raphnet_win/raphnet_source
      </td>
      <td>
        <a href="https://www.raphnet-tech.com/products/adapter_manager/index.php">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        GameCube Memory Manager
      </td>
      <td>
        gcmm
      </td>
      <td>
        <a href="https://github.com/suloku/gcmm/releases">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Free PSX Boot (PlayStation 1)
      </td>
      <td>
        freePsxBoot
      </td>
      <td>
        <a href="https://github.com/brad-lin/FreePSXBoot/releases">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Unirom (PlayStation 1)
      </td>
      <td>
        unirom
      </td>
      <td>
        <a href="https://github.com/JonathanDotCel/unirom8_bootdisc_and_firmware_for_ps1/releases">Link</a>
      </td>
    </tr>
    <tr>
      <td>
        Hackchi2 (Nes Classic, SNES Classic, Genesis Mini)
      </td>
      <td>
        hackchi2_installer/hackchi2_portable
      </td>
      <td>
        <a href="https://github.com/teamshinkansen/hakchi2-ce/releases/">Link</a>
      </td>
    </tr>
     <tr>
      <td>
        Tinkup
      </td>
      <td>
        tinkup
      </td>
      <td>
        <a href="https://github.com/rmull/tinkup/releases/">Link</a>
      </td>
    </tr>
  </tbody>
</table>

### Devices

<table>
  <thead>
    <tr>
      <td>
        <strong>Devices</strong>
      </td>
      <td>
        <strong>Config file key</strong>
      </td>
      <td>
        <strong>Link</strong>
      </td>
      <td>Notes</td>
    </tr>
  <thead>
  <tbody>
    <tr>
      <td>
        Analogue Pocket
      </td>
      <td>
        analoguePocket
      </td>
      <td>
        <a href="https://www.analogue.co/support/pocket/firmware">Link</a>
      </td>
      <td></td>
    </tr>
    <tr>
      <td>
        Analogue Duo
      </td>
      <td>
        analogueDuo
      </td>
      <td>
        <a href="https://www.analogue.co/support/duo/firmware">Link</a>
      </td>
      <td></td>
    </tr>
    <tr>
      <td>
        Analogue Duo Jailbreak FW
      </td>
      <td>
        analogueDuoJailbreak
      </td>
      <td>
        <a href="https://github.com/analoguejb/Analogue-Duo-JB">Link</a>
      </td>
      <td></td>
    </tr>
    <tr>
      <td>
        PSX Pi SmbShare (Raspberry Pi and PlayStation 2)
      </td>
      <td>
        psxpismbshare
      </td>
      <td>
        <a href="https://github.com/toolboc/psx-pi-smbshare/releases">Link</a>
      </td>
      <td></td>
    </tr>
    <tr>
      <td>
        GB Interceptor (Game Boy)
      </td>
      <td>
        gbinterceptor
      </td>
      <td>
        <a href="https://github.com/Staacks/gbinterceptor/releases">Link</a>
      </td>
      <td></td>
    </tr>
    <tr>
      <td>
        RetroUSB AVS firmware (NES)
      </td>
      <td>
        retroUsbAvs
      </td>
      <td>
        <a href="https://www.retrousb.com/product_info.php?cPath=36&products_id=78&osCsid=2945a80f5930658692da052e1cfd53fd">Link</a>
      </td>
      <td>Needs RetroUSB Scoreboard software (retroUsbSb-win or retroUsbSb-mac in "software" section)</td>
    </tr>
    <tr>
      <td>
        RetroTink 5x Pro
      </td>
      <td>
        retroTink5xPro
      </td>
      <td>
        <a href="https://retrotink-llc.github.io/firmware/5x.html">Link</a>
      </td>
      <td></td>
    </tr>
    <tr>
      <td>
        RetroTink 4K
      </td>
      <td>
        retroTink4k
      </td>
      <td>
        <a href="https://retrotink-llc.github.io/firmware/4k.html">Link</a>
      </td>
      <td></td>
    </tr>
    <tr>
      <td>
        Pseudo Saturn Kai (Saturn)
      </td>
      <td>
        psKai
      </td>
      <td>
        <a href="https://ppcenter.webou.net/pskai/">Link</a>
      </td>
      <td>Saturn cartridge software for playing backups</td>
    </tr>
    <tr>
      <td>
        Pseudo Saturn Kai Menu (Saturn)
      </td>
      <td>
        psKaiMenu
      </td>
      <td>
        <a href="https://ppcenter.webou.net/pskai/">Link</a>
      </td>
      <td>Saturn ODE alt loader menu</td>
    </tr>
    <tr>
      <td>
        OSSC
      </td>
      <td>
        ossc_audio_en/ossc_audio_jp/ossc_noAudio_en/ossc_noAudio_jp
      </td>
      <td>
        <a href="https://www.niksula.hut.fi/~mhiienka/ossc/fw/">Link</a>
      </td>
      <td></td>
    </tr>
    <tr>
      <td>
        RGB Blaster
      </td>
      <td>
        rgbBlaster
      </td>
      <td>
        <a href="https://krikzz.com/pub/support/rgb-blaster/firmware/">Link</a>
      </td>
      <td></td>
    </tr>
  </tbody>
</table>

### Mods

<table>
  <thead>
    <tr>
      <td>
        <strong>Mods</strong>
      </td>
      <td>
        <strong>Config file key</strong>
      </td>
      <td>
        <strong>Link</strong>
      </td>
      <td>Notes</td>
    </tr>
  <thead>
  <tbody>
    <tr>
      <td>
        PicoBoot (GameCube mod chip)
      </td>
      <td>
        picoboot
      </td>
      <td>
        <a href="https://github.com/webhdx/PicoBoot/releases">Link</a>
      </td>
      <td></td>
    </tr>
    <tr>
      <td>
        Kunai GC (GameCube mod chip)
      </td>
      <td>
        kunaiGc
      </td>
      <td>
        <a href="https://github.com/KunaiGC/KunaiGC/releases">Link</a>
      </td>
      <td>Downloads binary files to be flashed and sd card zip file to be copied</td>
    </tr>
    <tr>
      <td>
        Retron 77 Firmware NG
      </td>
      <td>
        r77FwNg_sdcard/r77FwNg_uImage
      </td>
      <td>
        <a href="https://github.com/DirtyHairy/r77-firmware-ng">Link</a>
      </td>
      <td>Downloads full SD card image (_sdcard) or Stella binary and Linux image (uImage). For major version changes, use SD card image first. To update minor versions, you can just copy new uImage to your SD card.</td>
    </tr>
  </tbody>
</table>

---

## Notes

- To give myself a way to periodically check the scraping and GitHub URLs and logic, I setup Jest to run a series of tests that should alert me when the logic for a device no longer works. You can run these tests by running `npm t` in the command line. That said, you can also ignore `testConfigs/testConfig.json` as it is only meant for these tests and is filled with invalid version numbers to force the tests to run the scraping and GitHub API logic for each device.

For a list of devices and links that could not be added to the scripts for whatever reason, check out the table [HERE](./otherLinks.md).
