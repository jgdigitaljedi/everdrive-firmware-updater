import config from './config.json' assert { type: 'json' };
import { deleteNewFw } from './util/deleteDownloaded.mjs';
import checkEverdriveFw from './src/checkEverdrives.mjs';
import checkGcloaderFw from './src/checkGcloader.mjs';
import check64DriveFw from './src/check64Drive.mjs';
import copyConfigToUpdated from './util/copyConfigToUpdated.mjs';
import checkNeoPocketGd from './src/checkNeoPocketGd.mjs';
import checkFenrir from './src/checkFenrir.mjs';
import checkSatiator from './src/checkSatiator.mjs';
import checkXstation from './src/checkXstation.mjs';
import checkSwiss from './src/checkSwiss.mjs';
import checkAnaloguePocket from './src/checkAnaloguePocket.mjs';
import checkFreeMcBoot from './src/checkFreeMcBoot.mjs';
import checkFlashMasta from './src/checkFlashMasta.mjs';
import checkLtoFlash from './src/checkLtoFlash.mjs';
import checkHarmonyCart from './src/checkHarmonyCart.mjs';
import checkXPort from './src/checkXPort.mjs';
import checkTeos from './src/checkTeos.mjs';
import checkTonyHax from './src/checkTonyHax.mjs';
import checkOpl from './src/checkOpl.mjs';
import checkPsxPiSmbshare from './src/checkPsxPiSmbshare.mjs';
import checkGbInterceptor from './src/checkGbInterceptor.mjs';
import checkTurboEdPro from './src/checkTurboEdPro.mjs';
import checkRetroUsbAvs from './src/checkRetroUsbAvs.mjs';
import checkRetroUsbScorecard from './src/checkRetroUsbScoreboard.mjs';
import checkPicoBoot from './src/checkPicoBoot.mjs';
import checkRaphnet from './src/checkRaphnetAdapterManager.mjs';
import checkRetroTink5xPro from './src/checkRetroTink5xPro.mjs';
import checkGcmm from './src/checkGcMemoryManager.mjs';
import checkFreePsxBoot from './src/checkFreePsxBoot.mjs';
import checkUnirom from './src/checkUnirom.mjs';
import checkPsKai from './src/checkPsKai.mjs';
import checkHackchi2 from './src/checkHackchi2.mjs';
import checkOssc from './src/checkOssc.mjs';
import checkRgbBlaster from './src/checkRgbBlaster.mjs';
import checkKunaiGc from './src/checkKunaiGc.mjs';
import checkTinkup from './src/checkTinkup.mjs';
import checkR77FwNg from './src/checkR77FwNg.mjs';
import checkAnalogueDuo from './src/checkAnalogueDuo.mjs';
import checkRetroTink4k from './src/checkRetrotink4k.mjs';
import checkAnalogueDuoJailbreak from './src/checkAnalogueDuoJailbreak.mjs';

(async () => {
  // setup updatedConfig
  copyConfigToUpdated();

  // delete downloaded firmware first
  // await deleteNewFw();

  // check for EverDrive firmware updates
  if (Object.keys(config.everDrives)?.length > 0) {
    await checkEverdriveFw();
  }

  // check for GCLoader firmware updates
  if (config.odes?.gcloader) {
    await checkGcloaderFw();
  }

  // check for 64Drive firmware updates
  if (config.flashCarts?.hw2_64Drive || config.flashCarts?.hw1_64Drive) {
    await check64DriveFw();
  }

  // check for Neo Pocket GameDrive firmware updates
  if (config.flashCarts?.neoPocketGD) {
    await checkNeoPocketGd();
  }

  // check for Fenrir firmware updates
  if (config.odes?.fenrir) {
    await checkFenrir();
  }

  // check for Satiator firmware updates
  if (config.odes?.satiator) {
    await checkSatiator();
  }

  // check for XStation firmware updates
  if (config.odes?.xstation) {
    await checkXstation();
  }

  // check for Swiss updates for GC
  if (config.software?.swiss) {
    await checkSwiss();
  }

  // check for Analogue Pocket updates
  if (config.devices?.analoguePocket) {
    await checkAnaloguePocket();
  }

  // check for Free McBoot updates
  if (config.software?.freemcboot) {
    await checkFreeMcBoot();
  }

  // check for FlashMasta software by OS
  if (
    config.software?.flashmasta_mac ||
    config.software?.flashmasta_linux ||
    config.software?.flashmasta_windows
  ) {
    await checkFlashMasta();
  }

  // check for LTO Flash software by OS
  if (
    config.software?.ltoflash_mac ||
    config.software?.ltoflash_source ||
    config.software?.ltoflash_windows
  ) {
    await checkLtoFlash();
  }

  // check for Harmony Cart software by OS
  if (
    config.software?.harmonycart_mac ||
    config.software?.harmonycart_linux ||
    config.software?.harmonycart_windows ||
    config.software?.harmonycart_source
  ) {
    await checkHarmonyCart();
  }

  // check for XPort firmware updates
  if (config.odes?.xport) {
    await checkXPort();
  }

  // check for TEOS alt TurboEverdrive fw updates
  if (config.flashCarts?.teos) {
    await checkTeos();
  }

  // check for TonyHax software updates
  if (config.software?.tonyhax) {
    await checkTonyHax();
  }

  // check for Open PS2 Loader software updates
  if (config.software?.opl) {
    await checkOpl();
  }

  // check for PSX Pi SmbShare software updates
  if (config.devices?.psxpismbshare) {
    await checkPsxPiSmbshare();
  }

  // check for GB Interceptor firmware update
  if (config.devices?.gbinterceptor) {
    await checkGbInterceptor();
  }

  // check Turbo EverDrive Pro fw; not on Stone Age Gamer yet
  if (config.everDrives?.turboEdPro) {
    await checkTurboEdPro();
  }

  // check RetroUSB AVS firmware
  if (config.devices?.retroUsbAvs) {
    await checkRetroUsbAvs();
  }

  // check RetroUSB Scroeboard software
  if (config.software?.retroUsbSb_win || config.software?.retroUsbSb_mac) {
    await checkRetroUsbScorecard();
  }

  // check PicoBoot firmware
  if (config.mods?.picoboot) {
    await checkPicoBoot();
  }

  // check Raphnet Adapter Manager software
  if (config.software?.raphnet_win || config.software?.raphnet_source) {
    await checkRaphnet();
  }

  // check RetroTink 5x Pro firmware
  if (config.devices?.retroTink5xPro) {
    await checkRetroTink5xPro();
  }

  // check RetroTink 4K firmware
  if (config.devices?.retroTink4k) {
    await checkRetroTink4k();
  }

  // check GameCube Memory Manager software
  if (config.software?.gcmm) {
    await checkGcmm();
  }

  // check FreePSXBoot software
  if (config.software?.freePsxBoot) {
    await checkFreePsxBoot();
  }

  // check FreePSXBoot software
  if (config.software?.unirom) {
    await checkUnirom();
  }

  // check Pseudo Saturn Kai
  if (config.devices?.psKai) {
    await checkPsKai('psKai');
  }

  // check Pseudo Saturn Kai Menu
  if (config.devices?.psKaiMenu) {
    await checkPsKai('psKaiMenu');
  }

  // check Hackchi2
  if (config.software?.hackchi2_installer || config.software?.hackchi2_portable) {
    await checkHackchi2();
  }

  // check OSSC fw
  if (
    config.devices?.ossc_audio_en ||
    config.devices?.ossc_audio_jp ||
    config.devices?.ossc_noAudio_en ||
    config.devices?.ossc_noAudio_jp
  ) {
    await checkOssc();
  }

  // check RGB Blaster
  if (config.devices?.rgbBlaster) {
    await checkRgbBlaster();
  }

  // check Kunai GC
  if (config.mods?.kunaiGc) {
    await checkKunaiGc();
  }

  // check Tinkup
  if (config.software?.tinkup) {
    await checkTinkup();
  }

  // check Retron 77 Fw
  if (config.mods?.r77FwNg_sdcard || config.mods?.r77FwNg_uImage) {
    await checkR77FwNg();
  }

  // check for Analogue Duo updates
  if (config.devices?.analogueDuo) {
    await checkAnalogueDuo();
  }

  // check for Analogue Duo Jailbreak updates
  if (config.devices?.analogueDuoJailbreak) {
    await checkAnalogueDuoJailbreak();
  }
})();
