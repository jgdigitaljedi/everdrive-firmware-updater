export const statusEmo = {
  check: '\u{2705}',
  fire: '\u{1F525}',
  ex: '\u{274C}',
  package: '\u{1F4E6}',
  game: '\u{1F3AE}',
  up: '\u{2B06}',
  down: '\u{2B07}',
  trash: '\u{1F5D1}'
};
