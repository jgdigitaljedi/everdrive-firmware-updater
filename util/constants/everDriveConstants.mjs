export const shortnameToFull = {
  megaX: 'EverDrive Mega X',
  megaPro: 'EverDrive Mega Pro',
  megaV1: 'Mega EverDrive',
  superEd: 'Super EverDrive',
  fxPak: 'FXPAK/SD2SNES',
  ed64: 'EverDrive 64',
  ed64V1: 'EverDrive64 (v1)',
  gbEd: 'EverDrive-GB',
  edGbV1: 'EverDrive-GB (v1)',
  gbaEd: 'EverDrive-GBA',
  turboEd: 'Turbo EverDrive',
  turboEdV1: 'Turbo EverDrive (v1)',
  n8: 'EverDrive-N8',
  n8Pro: 'EverDrive-N8 Pro',
  masterEd: 'Master EverDrive',
  masterEdV1: 'Master EverDrive (v1)',
  edGg: 'EverDrive-GG'
};

export const edFwUrls = {
  megaX: {
    url: 'https://stoneagegamer.com/mega-everdrive-x-downloads.html',
    tableNum: 0
  },
  megaPro: {
    url: 'https://stoneagegamer.com/mega-everdrive-pro-downloads.html',
    tableNum: 0
  },
  megaV1: {
    url: 'https://stoneagegamer.com/legacy-everdrive-flash-cart-downloads.html',
    tableNum: 0,
    isLegacy: true
  },
  superEd: {
    url: 'https://stoneagegamer.com/super-everdrive-downloads.html',
    tableNum: 0
  },
  fxPak: {
    url: 'https://stoneagegamer.com/fxpak-sd2snes-downloads.html',
    tableNum: 1
  },
  ed64: {
    url: 'https://stoneagegamer.com/everdrive64-downloads.html',
    tableNum: 0
  },
  ed64V1: {
    url: 'https://stoneagegamer.com/legacy-everdrive-flash-cart-downloads.html',
    tableNum: 2,
    isLegacy: true
  },
  gbEd: {
    url: 'https://stoneagegamer.com/everdrive-gb-downloads.html',
    tableNum: 0
  },
  edGbV1: {
    url: 'https://stoneagegamer.com/legacy-everdrive-flash-cart-downloads.html',
    tableNum: 4,
    isLegacy: true
  },
  gbaEd: {
    url: 'https://stoneagegamer.com/everdrive-gba-downloads.html',
    tableNum: 0
  },
  turboEd: {
    url: 'https://stoneagegamer.com/turbo-everdrive-downloads.html',
    tableNum: 0
  },
  turboEdV1: {
    url: 'https://stoneagegamer.com/legacy-everdrive-flash-cart-downloads.html',
    tableNum: 5,
    isLegacy: true
  },
  n8: {
    url: 'https://stoneagegamer.com/everdrive-n8-downloads.html',
    tableNum: 0
  },
  n8Pro: {
    url: 'https://stoneagegamer.com/everdrive-n8-pro-downloads.html',
    tableNum: 0
  },
  masterEd: {
    url: 'https://stoneagegamer.com/master-everdrive-downloads.html',
    tableNum: 0
  },
  masterEdV1: {
    url: 'https://stoneagegamer.com/legacy-everdrive-flash-cart-downloads.html',
    tableNum: 6,
    isLegacy: true
  },
  edGg: {
    url: 'https://stoneagegamer.com/everdrive-gg-downloads.html',
    tableNum: 0
  }
};
