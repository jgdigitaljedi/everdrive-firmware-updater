const otherFwUrls = {
  gcloader: 'https://gc-loader.com/firmware-updates',
  drive64: 'http://64drive.retroactive.be/support.php',
  drive64Dl: 'http://64drive.retroactive.be/',
  neoPocketGD:
    'https://www.retrohq.co.uk/products/neo-geo-pocket-neopocket-gamedrive-flash-cartridge',
  fenrir: 'https://www.fenrir-ode.fr/firmware/#version-history',
  fenrirDl: 'https://www.fenrir-ode.fr/',
  satiator: 'https://info.satiator.net/firmware/',
  satiatorDl: 'https://info.satiator.net/firmware/',
  xstation: 'https://github.com/x-station/xstation-releases/releases',
  pocket: 'https://www.analogue.co/support/pocket/firmware',
  duo: 'https://www.analogue.co/support/duo/firmware',
  freemcboot: 'https://israpps.github.io/FreeMcBoot-Installer/test/8_Downloads.html',
  r4sdhc: 'https://www.r43ds.org/pages/R4-3DS-Dual-Core-Firmware.html',
  flashmasta: 'https://www.flashmasta.com/software-downloads/',
  ltoflash: 'http://www.intvfunhouse.com/intvfunhouse/ltoflash/',
  harmonycart: 'https://forums.atariage.com/topic/156500-latest-harmony-cart-software/',
  turboEdPro: 'https://krikzz.com/pub/support/turbo-everdrive/pro-series/firmware/',
  retroUsbAvs: 'https://www.retrousb.com/product/avs/21',
  raphnetAdapterManager: 'https://www.raphnet-tech.com/products/adapter_manager/index.php',
  retroTink5xPro: 'https://retrotink-llc.github.io/firmware/5x.html',
  retroTink4k: 'https://retrotink-llc.github.io/firmware/4k.html',
  psKai: 'https://ppcenter.webou.net/pskai/',
  ossc: 'https://www.niksula.hut.fi/~mhiienka/ossc/fw/',
  rgbBlaster: 'https://krikzz.com/pub/support/rgb-blaster/firmware/'
};

export default otherFwUrls;
