const genericGhDevices = {
  swiss: {
    owner: 'emukidid',
    repo: 'swiss-gc',
    configSection: 'software',
    configKey: 'swiss',
    itemName: 'Swiss',
    filePathMid: 'software/Swiss_',
    ext: '.zip'
  },
  teos: {
    owner: 'jbrandwood',
    repo: 'teos',
    configSection: 'flashCarts',
    configKey: 'teos',
    itemName: 'TEOS',
    filePathMid: 'flashcarts/TEOS_',
    ext: '.zip'
  },
  xstation: {
    owner: 'x-station',
    repo: 'xstation-releases',
    configSection: 'odes',
    configKey: 'xstation',
    itemName: 'XStation',
    filePathMid: 'ODEs/XStation_',
    ext: '.zip'
  },
  xport: {
    owner: 'fixelsan',
    repo: '3do-ode-firmware',
    configSection: 'odes',
    configKey: 'xport',
    itemName: 'XPort',
    filePathMid: 'ODEs/XPort_',
    ext: '.xpu'
  },
  tonyhax: {
    owner: 'socram8888',
    repo: 'tonyhax',
    configSection: 'software',
    configKey: 'tonyhax',
    itemName: 'TonyHax',
    filePathMid: 'software/TonyHax_',
    ext: '.zip'
  },
  freePsxBoot: {
    owner: 'brad-lin',
    repo: 'FreePSXBoot',
    configSection: 'software',
    configKey: 'freePsxBoot',
    itemName: 'FreePSXBoot',
    filePathMid: 'software/FreePSXBoot_',
    ext: '.zip'
  },
  gcmm: {
    owner: 'suloku',
    repo: 'gcmm',
    configSection: 'software',
    configKey: 'gcmm',
    itemName: 'GameCube Memory Manager',
    filePathMid: 'software/GCMM_',
    ext: '.zip'
  },
  psxpismbshare: {
    owner: 'toolboc',
    repo: 'psx-pi-smbshare',
    configSection: 'devices',
    configKey: 'psxpismbshare',
    itemName: 'PSX Pi SmbShare',
    filePathMid: 'devices/PsxPiSmbshare_',
    ext: '.tar.gz'
  },
  picoboot: {
    owner: 'webhdx',
    repo: 'PicoBoot',
    configSection: 'mods',
    configKey: 'picoboot',
    itemName: 'PicoBoot',
    filePathMid: 'mods/PicoBoot_',
    ext: '.uf2'
  },
  unirom: {
    owner: 'JonathanDotCel',
    repo: 'unirom8_bootdisc_and_firmware_for_ps1',
    configSection: 'software',
    configKey: 'unirom',
    itemName: 'Unirom',
    filePathMid: 'software/Unirom_',
    ext: '.zip'
  },
  analogueDuoJailbreak: {
    owner: 'analoguejb',
    repo: 'Analogue-Duo-JB',
    configSection: 'devices',
    configKey: 'analogueDuoJailbreak',
    itemName: 'Analogue Duo Jailbreak',
    filePathMid: 'software/AnalogueDuoJailbreak_',
    ext: '.zip'
  }
};

export default genericGhDevices;
