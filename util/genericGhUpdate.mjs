import chalk from 'chalk';
import config from '../config.json' assert { type: 'json' };
import { downloadFw } from './downloadNewFile.mjs';
import { getGithubData } from './githubHelper.mjs';
import { updateDlLinkList } from './updateDlLinkList.mjs';
import { writeUpdatedConfig } from './writeUpdatedConfig.mjs';
import { statusEmo } from './constants/statusEmojies.mjs';

async function genericGhCall({ owner, repo, configSection, configKey, itemName }) {
  try {
    const { latest, dlLink } = await getGithubData(owner, repo);

    if (config[configSection][configKey] !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${
            statusEmo.up
          } ${itemName.toUpperCase()} VERSION UPDATE AVAILABLE (${latest}): ${dlLink}`
        )
      );
      await updateDlLinkList(configKey, dlLink);
      return { name: configKey, link: dlLink, version: latest };
    } else {
      console.log(chalk.cyan(`${statusEmo.check} ${itemName} is currently up to date.`));
      return null;
    }
  } catch (error) {
    console.log('owner', owner);
    return { error };
  }
}

export async function genericGhUpdate(device) {
  try {
    const newFw = await genericGhCall(device);
    if (newFw && !newFw.error) {
      await writeUpdatedConfig(device.configKey, device.configSection, newFw.version);
      const downloaded = await downloadFw(
        newFw,
        `../downloads/${device.filePathMid}${newFw.version}${device.ext}`,
        `${statusEmo.fire} Error downloading latest version of ${device.itemName}`
      );
      if (downloaded) {
        console.log(
          chalk.magenta(`${statusEmo.game} Downloaded latest version of ${device.itemName}!\n`)
        );
      }
    }
  } catch (error) {
    console.log(
      chalk.red.bold(`${statusEmo.fire} Error fetching update info for ${device.itemName}`)
    );
  }
}
