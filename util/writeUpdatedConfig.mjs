import chalk from 'chalk';
import { readFileSync, writeFileSync } from 'node:fs';
import { dirname, join } from 'node:path';
import { fileURLToPath } from 'node:url';
import { statusEmo } from './constants/statusEmojies.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export const writeUpdatedConfig = async (key, subKey, version) => {
  try {
    const updatedConfigPath = join(__dirname, '../data/updatedConfig.json');
    const updatedConfig = readFileSync(updatedConfigPath, 'utf-8');
    const parsed = JSON.parse(updatedConfig);
    parsed[subKey][key] = version;
    writeFileSync(updatedConfigPath, JSON.stringify(parsed, null, 2), 'utf-8');
    return Promise.resolve();
  } catch (error) {
    console.log(chalk.red.bold(`${statusEmo.fire} Error writing updatedConfig.json: `, error));
    return Promise.reject(error);
  }
};
