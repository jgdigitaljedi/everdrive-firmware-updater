import { pipeline } from 'node:stream';
import { promisify } from 'node:util';
import fetch from 'node-fetch';
import chalk from 'chalk';
import { createWriteStream, readdirSync } from 'node:fs';
import { dirname, join } from 'node:path';
import { fileURLToPath } from 'node:url';
import { deleteSingle } from './deleteDownloaded.mjs';
import { statusEmo } from './constants/statusEmojies.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const downloadFile = async (ed, filePath, errorMessage) => {
  const streamPipeline = promisify(pipeline);
  const response = await fetch(ed.link);
  if (!response.ok) {
    console.log(chalk.red(errorMessage));
    return Promise.resolve();
  }
  await streamPipeline(response.body, createWriteStream(join(__dirname, filePath)));
};

const checkForFw = async (ed, dirPath, filePrefix) => {
  const files = readdirSync(join(__dirname, dirPath), 'utf-8');

  const hasFw = files.filter((f) => f.indexOf(filePrefix) >= 0).length > 0;
  const hasVersion = files.filter((f) => f.indexOf(`${filePrefix}_${ed.version}`) >= 0).length > 0;

  if (hasVersion) {
    return 'sameVersion';
  } else if (hasFw) {
    return 'diffVersion';
  }
  return null;
};

export const downloadFw = async (ed, filePath, errorMessage) => {
  const filePrefix = filePath.split('/').pop().split('_')[0];
  const dirPath = filePath.split('/').splice(0, 3).join('/');
  const downloaded = await checkForFw(ed, dirPath, filePrefix);

  if (downloaded === 'sameVersion') {
    console.log(
      chalk.magenta.italic(
        `${statusEmo.down} Already downloaded latest version of ${filePrefix}.\n`
      )
    );
    return Promise.resolve(false);
  }
  if (downloaded === 'diffVersion') {
    try {
      await deleteSingle(dirPath, filePrefix, ed.version);
      console.log(chalk.green(`${statusEmo.trash} Deleted old version of ${filePrefix}`));
    } catch (err) {
      console.log(chalk.red(`${statusEmo.fire} Error deleting old version of ${filePrefix}`, err));
    }
  }
  await downloadFile(ed, filePath, errorMessage);
  return Promise.resolve(true);
};
