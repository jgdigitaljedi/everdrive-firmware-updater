import { dirname, extname, join } from 'node:path';
import { readdirSync, unlinkSync } from 'node:fs';
import { fileURLToPath } from 'node:url';
import { unlink } from 'node:fs/promises';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const subdirs = ['flashcarts', 'ODEs', 'software', 'devices', 'mods'];
const extensions = [
  '.zip',
  '.rpk',
  '.duo',
  '.exe',
  '.gz',
  '.dmg',
  '.deb',
  '.xpu',
  '.7z',
  '.uf2',
  '.csv',
  '.efu',
  '.bin'
];

export async function deleteZipFiles(subdir) {
  const dir = readdirSync(join(`${__dirname}`, '../downloads', subdir)) || [];
  const zipFiles = dir.filter((file) => extensions.indexOf(extname(file)) >= 0) || [];
  const zLen = zipFiles.length;
  for (let i = 0; i < zLen; i++) {
    unlinkSync(join(__dirname, '../downloads', subdir, zipFiles[i]), (err) => {
      if (err) {
        console.log(err);
      }
    });
    if (i + 1 === zLen) return Promise.resolve();
  }
}

export async function deleteNewFw() {
  const sdLen = subdirs.length;
  for (let i = 0; i < subdirs.length; i++) {
    await deleteZipFiles(subdirs[i]);
    if (i + 1 === sdLen) return Promise.resolve();
  }
}

export const deleteSingle = (dirPath, filePrefix, latest) => {
  const dir = readdirSync(join(`${__dirname}`, dirPath)) || [];
  const matchToDelete = dir.filter(
    (file) => file.indexOf(filePrefix) >= 0 && file.indexOf(latest < 0)
  );

  if (matchToDelete?.length > 0) {
    return unlink(join(__dirname, dirPath, matchToDelete[0]));
  } else {
    return Promise.resolve();
  }
};
