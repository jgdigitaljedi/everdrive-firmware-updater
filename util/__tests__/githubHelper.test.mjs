import { getGithubData } from '../githubHelper.mjs';
import allConfig from '../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../data/dlLinkList.json' assert { type: 'json' };

// just doing loose checks to make sure these props come back from the call.
// tests for each of the calls will happen in their own file.

describe('github endpoints', () => {
  it('should get a response for the Swiss GitHub call', async () => {
    const { latest, dlLink } = await getGithubData('emukidid', 'swiss-gc');
    expect(latest).toBe(allConfig.software.swiss);
    expect(dlLink).toBe(dlLinks.swiss);
  });

  it('should get a response for the XStation GitHub call', async () => {
    const { latest, dlLink } = await getGithubData('x-station', 'xstation-releases');
    expect(latest).toBe(allConfig.odes.xstation);
    expect(dlLink).toBe(dlLinks.xstation);
  });

  it('should get a response for the Harmony Cart GitHub call', async () => {
    const { latest, dlLink, assets } = await getGithubData('sa666666', 'harmonycart', true);
    expect(latest).toBe(allConfig.software.harmonycart_linux);
    expect(dlLink.includes('https://')).toBe(true);
    expect(assets.length).toBe(5);
  });

  it('should get a response for the XPort GitHub call', async () => {
    const { latest, dlLink } = await getGithubData('fixelsan', '3do-ode-firmware');
    expect(latest).toBe(allConfig.odes.xport);
    expect(dlLink).toBe(dlLinks.xport);
  });

  it('should get a response for the TEOS GitHub call', async () => {
    const { latest, dlLink } = await getGithubData('jbrandwood', 'teos');
    expect(latest).toBe(allConfig.flashCarts.teos);
    expect(dlLink).toBe(dlLinks.teos);
  });

  it('should get a response for the TonyHax GitHub call', async () => {
    const { latest, dlLink } = await getGithubData('socram8888', 'tonyhax');
    expect(latest).toBe(allConfig.software.tonyhax);
    expect(dlLink).toBe(dlLinks.tonyhax);
  });

  it('should get a response for the Open PS2 Loader GitHub call', async () => {
    const { latest, dlLink, assets } = await getGithubData('ps2homebrew', 'Open-PS2-Loader', true);
    expect(latest).toBe(allConfig.software.opl);
    expect(dlLink.includes('https://')).toBe(true);
    expect(assets.length).toBe(5);
  });

  it('should get a response for the PSX Pi SmbShare GitHub call', async () => {
    const { latest, dlLink } = await getGithubData('toolboc', 'psx-pi-smbshare');
    expect(latest).toBe(allConfig.devices.psxpismbshare);
    expect(dlLink).toBe(dlLinks.psxpismbshare);
  });

  it('should get a response for the GB Interceptor GitHub call', async () => {
    const { latest, dlLink, assets } = await getGithubData('Staacks', 'gbinterceptor', true);
    expect(latest).toBe(allConfig.devices.gbinterceptor);
    expect(dlLink.includes('https://')).toBe(true);
    expect(assets.length).toBe(4);
  });

  it('should get a response for the PicoBoot fw GitHub call', async () => {
    const { latest, dlLink } = await getGithubData('webhdx', 'PicoBoot');
    expect(latest).toBe(allConfig.mods.picoboot);
    expect(dlLink).toBe(dlLinks.picoboot);
  });

  it('should get a response for the GameCube Memory Manager GitHub call', async () => {
    const { latest, dlLink } = await getGithubData('suloku', 'gcmm');
    expect(latest).toBe(allConfig.software.gcmm);
    expect(dlLink).toBe(dlLinks.gcmm);
  });

  it('should get a response for the FreePSXBoot GitHub call', async () => {
    const { latest, dlLink } = await getGithubData('brad-lin', 'FreePSXBoot');
    expect(latest).toBe(allConfig.software.freePsxBoot);
    expect(dlLink).toBe(dlLinks.freePsxBoot);
  });

  it('should get a response for the Unirom GitHub call', async () => {
    const { latest, dlLink } = await getGithubData(
      'JonathanDotCel',
      'unirom8_bootdisc_and_firmware_for_ps1'
    );
    expect(latest).toBe(allConfig.software.unirom);
    expect(dlLink).toBe(dlLinks.unirom);
  });

  it('should get a response for the GB Interceptor GitHub call', async () => {
    const { latest, dlLink, assets } = await getGithubData('teamshinkansen', 'hakchi2-ce', true);
    expect(latest).toBe(allConfig.software.hackchi2_installer);
    expect(dlLink.includes('https://')).toBe(true);
    expect(assets.length).toBe(2);
  });

  it('should get a response for the Kunai GC GitHub call', async () => {
    const { latest, dlLink, assets } = await getGithubData('KunaiGC', 'KunaiGC', true);
    expect(latest).toBe(allConfig.mods.kunaiGc);
    expect(dlLink.includes('https://')).toBe(true);
    expect(assets.length).toBe(6);
  });
});
