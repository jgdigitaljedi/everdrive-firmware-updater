import { Octokit } from '@octokit/rest';

export async function getGithubData(owner, repo, returnAssetsArr) {
  try {
    const octokit = new Octokit({ auth: process.env.GITHUB_KEY });

    const releases = await octokit.request('GET /repos/{owner}/{repo}/releases/latest', {
      owner,
      repo,
      headers: {
        'X-GitHub-Api-Version': '2022-11-28'
      }
    });

    const latest = releases.data.tag_name;
    const dlLink = releases.data.assets[0].browser_download_url;

    return { latest, dlLink, assets: returnAssetsArr ? releases.data.assets : undefined };
  } catch (error) {
    return { error };
  }
}
