/*
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/configuration
 */

module.exports = {
  testEnvironment: 'node',
  testMatch: ['**/__tests__/**/*.(mjs)', '**/?(*.)+(test).(mjs)'],
  transform: {
    '^.+\\.(js|mjs)$': 'babel-jest'
  },
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/util/$1',
    'config.json': '<rootDir>/testConfigs/testConfig.json'
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/']
};
