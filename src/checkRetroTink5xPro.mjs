import chalk from 'chalk';
import { getRetroTink5xProFw } from './deviceLogic/checkRetroTink5xProFw.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkRetroTink5xPro = async () => {
  try {
    const newFw = await getRetroTink5xProFw();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('retroTink5xPro', 'devices', newFw.version);
      await downloadFw(
        newFw,
        `../downloads/devices/retroTink5xPro_${newFw.version}.zip`,
        `${statusEmo.fire} Error downloading latest version of RetroTink 5x Pro firmware`
      );
      console.log(
        chalk.magenta(`${statusEmo.game} Downloaded latest version of RetroTink 5x Pro firmware!\n`)
      );
    }
  } catch (error) {
    console.log(
      chalk.red.bold(`${statusEmo.fire} Error fetching update info for RetroTink 5x Pro firmware`)
    );
  }
};

export default checkRetroTink5xPro;
