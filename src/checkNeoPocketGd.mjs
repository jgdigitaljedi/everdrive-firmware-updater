import chalk from 'chalk';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { getNeoPocketGdFw } from './deviceLogic/checkNeoPocketGdFw.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkNeoPocketGd = async () => {
  try {
    const newFw = await getNeoPocketGdFw();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('neoPocketGD', 'flashCarts', newFw.version);
      const downloaded = await downloadFw(
        newFw,
        `../downloads/flashcarts/neoPocketGD_${newFw.version}.zip`,
        `${statusEmo.fire} Error downloading firmware for Neo Pocket GameDrive`
      );
      if (downloaded) {
        console.log(
          chalk.magenta(`${statusEmo.game} Downloaded firmware for your Neo Pocket GameDrive!\n`)
        );
      }
    }
  } catch (error) {
    console.log(
      chalk.red.bold(`${statusEmo.fire} Error fetching update info for Neo Pocket GameDrive`)
    );
  }
};

export default checkNeoPocketGd;
