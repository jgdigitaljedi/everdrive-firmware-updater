import chalk from 'chalk';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';
import { getRetroTink4kFw } from './deviceLogic/checkRetroTink4kFw.mjs';

const checkRetroTink4k = async () => {
  try {
    const newFw = await getRetroTink4kFw();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('retroTink4k', 'devices', newFw.version);
      await downloadFw(
        newFw,
        `../downloads/devices/retroTink4k_${newFw.version}.zip`,
        `${statusEmo.fire} Error downloading latest version of RetroTink 4K  firmware`
      );
      console.log(
        chalk.magenta(`${statusEmo.game} Downloaded latest version of RetroTink 4K firmware!\n`)
      );
    }
  } catch (error) {
    console.log(
      chalk.red.bold(`${statusEmo.fire} Error fetching update info for RetroTink 4K firmware`)
    );
  }
};

export default checkRetroTink4k;
