import genericGhDevices from '../util/constants/genericGhDevices.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkAnalogueDuoJailbreak = async () => {
  const device = genericGhDevices.analogueDuoJailbreak;
  await genericGhUpdate(device);
};

export default checkAnalogueDuoJailbreak;