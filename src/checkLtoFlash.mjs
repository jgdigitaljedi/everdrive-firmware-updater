import chalk from 'chalk';
import config from '../config.json' assert { type: 'json' };
import { getLtoFlashVersion } from './deviceLogic/checkLtoFlashVersion.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkLtoFlash = async () => {
  const ltoNames = ['ltoflash_mac', 'ltoflash_source', 'ltoflash_windows'];
  const lto = ltoNames
    .map((key) => ({ name: key, version: config.software[key] || undefined }))
    .filter((i) => !!i.version);
  for (let i = 0; i < lto.length; i++) {
    const current = lto[i];
    try {
      const newFw = await getLtoFlashVersion(current);
      if (newFw && !newFw.error) {
        await writeUpdatedConfig(current.name, 'software', newFw.version);
        await downloadFw(
          newFw,
          `../downloads/software/${current.name}_${newFw.version}.zip`,
          `${statusEmo.fire} Error downloading software for ${current.name}`
        );
        console.log(
          chalk.magenta(
            `${statusEmo.game} Downloaded LTO Flash user interface software: ${current.name}!\n`
          )
        );
      }
    } catch (error) {
      console.log(
        chalk.red.bold(`${statusEmo.fire} Error fetching update info for ${current.name}`)
      );
    }
  }
};

export default checkLtoFlash;
