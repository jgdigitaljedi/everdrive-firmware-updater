import chalk from 'chalk';
import config from '../config.json' assert { type: 'json' };
import { getFirmwareVersion } from './deviceLogic/checkEverdriveFirmware.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { shortnameToFull } from '../util/constants/everDriveConstants.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkEverdriveFw = async () => {
  const eds = Object.keys(config.everDrives)
    .filter((key) => key !== 'turboEdPro')
    .map((key) => {
      return {
        name: key,
        version: config.everDrives[key]
      };
    });
  for (let i = 0; i < eds.length; i++) {
    try {
      const current = eds[i];
      const newFw = await getFirmwareVersion(current);
      if (newFw && !newFw.error) {
        await writeUpdatedConfig(eds.name, 'everDrives', newFw.version);
        const downloaded = await downloadFw(
          newFw,
          `../downloads/flashcarts/${current.name}_${newFw.version}.zip`,
          `${statusEmo.fire} Error downloading firmware for ${current.name}`
        );
        if (downloaded) {
          console.log(
            chalk.magenta(
              `${statusEmo.game} Downloaded firmware for your ${shortnameToFull[eds[i].name]}!\n`
            )
          );
        }
      }
    } catch (error) {
      console.log(
        chalk.red.bold(`${statusEmo.fire} Error fetching update info for ${eds[i].name}: ${error}`)
      );
    }
  }
};

export default checkEverdriveFw;
