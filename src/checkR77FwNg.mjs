import chalk from 'chalk';
import config from '../config.json' assert { type: 'json' };
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';
import { getR77FwNgVersion } from './deviceLogic/checkR77FwNgFw.mjs';

const getFileName = (newFw) => {
  if (newFw.name === 'r77FwNg_sdcard') {
    return `${newFw.name}_${newFw.version}${newFw.ext}`;
  }
  return 'uImage';
};

const checkR77FwNg = async () => {
  const r77Names = ['r77FwNg_sdcard', 'r77FwNg_uImage'];
  const r77 = r77Names
    .map((key) => ({ name: key, version: config.mods[key] || undefined }))
    .filter((i) => !!i.version);
  for (let i = 0; i < r77.length; i++) {
    const current = r77[i];
    try {
      const newFw = await getR77FwNgVersion(current);
      if (newFw && !newFw.error) {
        await writeUpdatedConfig(current.name, 'mods', newFw.version);
        await downloadFw(
          newFw,
          `../downloads/mods/${getFileName(newFw)}`,
          `${statusEmo.fire} Error downloading software for ${current.name}`
        );
        console.log(
          chalk.magenta(`${statusEmo.game} Downloaded Retron 77 Firmware NG: ${current.name}!\n`)
        );
      }
    } catch (error) {
      console.log(
        chalk.red.bold(`${statusEmo.fire} Error fetching update info for ${current.name}`)
      );
    }
  }
};

export default checkR77FwNg;
