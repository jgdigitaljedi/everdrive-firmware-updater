import genericGhDevices from '../util/constants/genericGhDevices.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkTonyHax = async () => {
  const device = genericGhDevices.tonyhax;
  await genericGhUpdate(device);
};

export default checkTonyHax;
