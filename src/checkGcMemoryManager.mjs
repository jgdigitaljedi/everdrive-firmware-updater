import genericGhDevices from '../util/constants/genericGhDevices.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkGcmm = async () => {
  const device = genericGhDevices.gcmm;
  await genericGhUpdate(device);
};

export default checkGcmm;
