import chalk from 'chalk';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { getSatiatorFw } from './deviceLogic/checkSatiatorFw.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkSatiator = async () => {
  try {
    const newFw = await getSatiatorFw();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('satiator', 'odes', newFw.version);
      const downloaded = await downloadFw(
        newFw,
        `../downloads/ODEs/satiator_${newFw.version}.zip`,
        `${statusEmo.fire} Error downloading firmware for Satiator`
      );
      if (downloaded) {
        console.log(chalk.magenta(`${statusEmo.game} Downloaded firmware for your Satiator!\n`));
      }
    }
  } catch (error) {
    console.log(chalk.red.bold(`${statusEmo.fire} Error fetching update info for Satiator`));
  }
};

export default checkSatiator;
