import chalk from 'chalk';
import { getOplUpdate } from './deviceLogic/checkOplUpdate.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkOpl = async () => {
  try {
    const newFw = await getOplUpdate();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('opl', 'software', newFw.version);
      const downloads = newFw.dlList;
      for (let i = 0; i < downloads.length; i++) {
        await downloadFw(
          downloads[i],
          `../downloads/software/OPL_${downloads[i].name}.7z`,
          `${statusEmo.fire} Error downloading latest version of Open PS2 Loader software`
        );
      }
      console.log(
        chalk.magenta(
          `${statusEmo.game} Downloaded files for latest version of Open PS2 Loader software!\n`
        )
      );
    }
  } catch (error) {
    console.log(
      chalk.red.bold(`${statusEmo.fire} Error fetching update info for Open PS2 Loader software`)
    );
  }
};

export default checkOpl;
