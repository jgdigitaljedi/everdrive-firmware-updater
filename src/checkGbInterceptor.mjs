import chalk from 'chalk';
import { getGbInterceptorUpdate } from './deviceLogic/checkGbInterceptorUpdate.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkGbInterceptor = async () => {
  try {
    const newFw = await getGbInterceptorUpdate();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('gbinterceptor', 'devices', newFw.version);
      const dlList = newFw.dlList;
      for (let i = 0; i < dlList.length; i++) {
        await downloadFw(
          newFw,
          `../downloads/software/GBInterceptor_${dlList[i].name}`,
          `${statusEmo.fire} Error downloading latest version of GB Interceptor firmware`
        );
      }
      console.log(
        chalk.magenta(`${statusEmo.game} Downloaded latest version of GB Interceptor firmware!\n`)
      );
    }
  } catch (error) {
    console.log(
      chalk.red.bold(`${statusEmo.fire} Error fetching firmware update info for GB Interceptor`)
    );
  }
};

export default checkGbInterceptor;
