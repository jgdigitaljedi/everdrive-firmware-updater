import chalk from 'chalk';
import { getR4SdhcFw } from './deviceLogic/checkR4SdhcFw.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkR4Sdhc = async () => {
  try {
    const newFw = await getR4SdhcFw();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('r4sdhc', 'flashCarts', newFw.version);
      await downloadFw(
        newFw,
        `../downloads/flashcarts/r4sdhc_${newFw.version}.zip`,
        `${statusEmo.fire} Error downloading latest version of R4SDHC firmware`
      );
      console.log(
        chalk.magenta(`${statusEmo.game} Downloaded latest version of R4SDHC firmware!\n`)
      );
    }
  } catch (error) {
    console.log(chalk.red.bold(`${statusEmo.fire} Error fetching update info for R4SDHC firmware`));
  }
};

export default checkR4Sdhc;
