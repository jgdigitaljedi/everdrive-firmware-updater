import { Octokit } from '@octokit/rest';
import config from '../config.json' assert { type: 'json' };
import chalk from 'chalk';
import { statusEmo } from '../util/constants/statusEmojies.mjs';
import { updateDlLinkList } from '../util/updateDlLinkList.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';

const checkTinkup = async () => {
  try {
    const octokit = new Octokit({ auth: process.env.GITHUB_KEY });

    const releases = await octokit.request('GET /repos/{owner}/{repo}/releases/latest', {
      owner: 'rmull',
      repo: 'tinkup',
      headers: {
        'X-GitHub-Api-Version': '2022-11-28'
      }
    });

    const latest = releases.data.tag_name;
    const dlLink = releases.data.zipball_url;

    if (config.software.tinkup !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} TINKUP VERSION UPDATE AVAILABLE (${latest}): ${dlLink}`
        )
      );
      await updateDlLinkList('tinkup', dlLink);
      const newFw = { name: 'tinkup', link: dlLink, version: latest };

      if (newFw && !newFw.error) {
        await writeUpdatedConfig('tinkup', 'software', newFw.version);
        const downloaded = await downloadFw(
          newFw,
          `../downloads/software/Tinkup_${newFw.version}.zip`,
          `${statusEmo.fire} Error downloading latest version of Tinkup`
        );
        if (downloaded) {
          console.log(chalk.magenta(`${statusEmo.game} Downloaded latest version of Tinkup!\n`));
        }
      }
    } else {
      console.log(chalk.cyan(`${statusEmo.check} Tinkup is currently up to date.`));
      return null;
    }
  } catch (error) {
    console.error('tinkup error', error);
    return { error };
  }
};

export default checkTinkup;
