import chalk from 'chalk';
import { getTurboEdProFw } from './deviceLogic/checkTurboEdProFw.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkTurboEdPro = async () => {
  try {
    const newFw = await getTurboEdProFw();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('turboEdPro', 'everDrives', newFw.version);
      await downloadFw(
        newFw,
        `../downloads/flashcarts/TurboEdPro_${newFw.version}.efu`,
        `${statusEmo.fire} Error downloading latest version of Turbo EverDrive Pro firmware`
      );
      console.log(
        chalk.magenta(
          `${statusEmo.game} Downloaded latest version of Turbo EverDrive Pro firmware!\n`
        )
      );
    }
  } catch (error) {
    console.log(
      chalk.red.bold(
        `${statusEmo.fire} Error fetching update info for Turbo EverDrive Pro firmware`
      )
    );
  }
};

export default checkTurboEdPro;
