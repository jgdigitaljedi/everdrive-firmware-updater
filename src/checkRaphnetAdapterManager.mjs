import chalk from 'chalk';
import config from '../config.json' assert { type: 'json' };
import { getRaphnetVersion } from './deviceLogic/checkRaphnetVersion.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkRaphnet = async () => {
  const rnNames = ['raphnet_win', 'raphnet_source'];
  const rn = rnNames
    .map((key) => ({ name: key, version: config.software[key] || undefined }))
    .filter((i) => !!i.version);
  for (let i = 0; i < rn.length; i++) {
    const current = rn[i];
    try {
      const newFw = await getRaphnetVersion(current);
      if (newFw && !newFw.error) {
        await writeUpdatedConfig(current.name, 'software', newFw.version);
        await downloadFw(
          newFw,
          `../downloads/software/${current.name}_${newFw.version}${newFw.ext}`,
          `${statusEmo.fire} Error downloading software for ${current.name}`
        );
        console.log(
          chalk.magenta(
            `${statusEmo.game} Downloaded Raphnet Adapter Manager software: ${current.name}!\n`
          )
        );
      }
    } catch (error) {
      console.log(
        chalk.red.bold(`${statusEmo.fire} Error fetching update info for ${current.name}`)
      );
    }
  }
};

export default checkRaphnet;
