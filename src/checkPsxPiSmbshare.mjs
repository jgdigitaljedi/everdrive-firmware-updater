import genericGhDevices from '../util/constants/genericGhDevices.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkPsxPiSmbshare = async () => {
  const device = genericGhDevices.psxpismbshare;
  await genericGhUpdate(device);
};

export default checkPsxPiSmbshare;
