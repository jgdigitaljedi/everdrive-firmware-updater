import chalk from 'chalk';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { getRgbBlasterFw } from './deviceLogic/checkRgbBlasterFw.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkRgbBlaster = async () => {
  try {
    const newFw = await getRgbBlasterFw();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('rgbBlaster', 'devices', newFw.version);
      await downloadFw(
        newFw,
        `../downloads/devices/RGB-Blaster_${newFw.version}.zip`,
        `${statusEmo.fire} Error downloading latest version of RGB Blaster firmware`
      );
      console.log(
        chalk.magenta(`${statusEmo.game} Downloaded latest version of RGB Blaster firmware!\n`)
      );
    }
  } catch (error) {
    console.log(
      chalk.red.bold(`${statusEmo.fire} Error fetching update info for RGB Blaster firmware`)
    );
  }
};

export default checkRgbBlaster;
