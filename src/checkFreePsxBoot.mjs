import genericGhDevices from '../util/constants/genericGhDevices.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkFreePsxBoot = async () => {
  const device = genericGhDevices.freePsxBoot;
  await genericGhUpdate(device);
};

export default checkFreePsxBoot;
