import genericGhDevices from '../util/constants/genericGhDevices.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkXPort = async () => {
  const device = genericGhDevices.xport;
  await genericGhUpdate(device);
};

export default checkXPort;
