import chalk from 'chalk';
import config from '../config.json' assert { type: 'json' };
import { getHarmonyCartVersion } from './deviceLogic/checkHarmonyCartVersion.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkHarmonyCart = async () => {
  const hcNames = [
    'harmonycart_mac',
    'harmonycart_linux',
    'harmonycart_windows',
    'harmonycart_source'
  ];
  const hc = hcNames
    .map((key) => ({ name: key, version: config.software[key] || undefined }))
    .filter((i) => !!i.version);
  for (let i = 0; i < hc.length; i++) {
    const current = hc[i];
    try {
      const newFw = await getHarmonyCartVersion(current);
      if (newFw && !newFw.error) {
        await writeUpdatedConfig(current.name, 'software', newFw.version);
        await downloadFw(
          newFw,
          `../downloads/software/${current.name}_${newFw.version}${newFw.ext}`,
          `${statusEmo.fire} Error downloading software for ${current.name}`
        );
        console.log(
          chalk.magenta(`${statusEmo.game} Downloaded Harmony Cart software: ${current.name}!\n`)
        );
      }
    } catch (error) {
      console.log(
        chalk.red.bold(`${statusEmo.fire} Error fetching update info for ${current.name}`)
      );
    }
  }
};

export default checkHarmonyCart;
