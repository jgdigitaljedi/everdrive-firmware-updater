import chalk from 'chalk';
import { getGithubData } from '../../util/githubHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

const versionToAsset = {
  hackchi2_installer: {
    ext: '.exe',
    message: 'Installer',
    file: 'installer.exe'
  },
  hackchi2_portable: {
    ext: '.zip',
    message: 'Portable',
    file: 'portable.zip'
  }
};

export async function getHackchi2Version(which) {
  try {
    const { latest, assets } = await getGithubData('teamshinkansen', 'hakchi2-ce', true);
    const versionProps = versionToAsset[which.name];
    const dlForVersion = assets.filter((item) => item.name.includes(versionProps.file))[0]
      .browser_download_url;

    if (which.version !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${
            statusEmo.up
          } HACKCHI2 ${versionProps.message.toUpperCase()} SOFTWARE UPDATE AVAILABLE (${latest}): ${dlForVersion}`
        )
      );
      await updateDlLinkList(which.name, dlForVersion);
      return { name: which.name, link: dlForVersion, version: latest, ext: versionProps.ext };
    } else {
      console.log(
        chalk.cyan(
          `${statusEmo.check} Hackchi2 ${versionProps.message} software is currently up to date.`
        )
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
