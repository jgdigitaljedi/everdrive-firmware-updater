import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getLtoFlashVersion } from '../checkLtoFlashVersion.mjs';

describe('checkLtoFlash', () => {
  const ltos = [
    { name: 'ltoflash_mac', version: 'v99' },
    { name: 'ltoflash_source', version: 'v99' },
    { name: 'ltoflash_windows', version: 'v99' }
  ];
  test.each(ltos)('should get the expected results from the %p.name calls', async (lto) => {
    const result = await getLtoFlashVersion(lto);
    expect(result.name).toBe(lto.name);
    expect(result.link).toBe(dlLinks[lto.name]);
    expect(result.version).toBe(allConfig.software[lto.name]);
  });
});
