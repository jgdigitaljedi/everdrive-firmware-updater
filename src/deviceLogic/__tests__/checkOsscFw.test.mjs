import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getOsscFw } from '../checkOsscFw.mjs';

describe('checkOsscFw', () => {
  const osscs = [
    { name: 'ossc_audio_en', version: 'v99' },
    { name: 'ossc_audio_jp', version: 'v99' },
    { name: 'ossc_noAudio_en', version: 'v99' },
    { name: 'ossc_noAudio_jp', version: 'v99' }
  ];
  test.each(osscs)(
    'should get the expected results from the %p.name calls',
    async (ossc) => {
      const result = await getOsscFw(ossc);
      expect(result.name).toBe(ossc.name);
      expect(result.link).toBe(dlLinks[ossc.name]);
      expect(result.version).toBe(allConfig.devices[ossc.name]);
    },
    10000
  );
});
