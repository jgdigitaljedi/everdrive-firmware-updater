import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getR77FwNgVersion } from '../checkR77FwNgFw.mjs';

const whichObj = {
  r77FwNg_uImage: {
    name: 'r77FwNg_uImage',
    version: 'v99'
  },
  r77FwNg_sdcard: {
    name: 'r77FwNg_sdcard',
    version: 'v99'
  }
};

describe('checkR77FwNgVersion', () => {
  it('should get the expected results from the Retron 77 Firmware NG uImage call', async () => {
    const result = await getR77FwNgVersion(whichObj.r77FwNg_uImage);
    expect(result.name).toBe('r77FwNg_uImage');
    expect(result.link).toBe(dlLinks.r77FwNg_uImage);
    expect(result.version).toBe(allConfig.mods.r77FwNg_uImage);
  }, 10000);

  it('should get the expected results from the Retron 77 Firmware NG SD card img call', async () => {
    const result = await getR77FwNgVersion(whichObj.r77FwNg_sdcard);
    expect(result.name).toBe('r77FwNg_sdcard');
    expect(result.link).toBe(dlLinks.r77FwNg_sdcard);
    expect(result.version).toBe(allConfig.mods.r77FwNg_sdcard);
  }, 10000);
});
