import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getRaphnetVersion } from '../checkRaphnetVersion.mjs';

describe('checkRaphnetVersion', () => {
  it('should get the expected results from the Raphnet Adapter Manager (windows) call', async () => {
    const result = await getRaphnetVersion({ name: 'raphnet_win', version: 'v99' });
    expect(result.name).toBe('raphnet_win');
    expect(result.link).toBe(dlLinks.raphnet_win);
    expect(result.version).toBe(allConfig.software.raphnet_win);
  }, 10000);

  it('should get the expected results from the Raphnet Adapter Manager (source) call', async () => {
    const result = await getRaphnetVersion({ name: 'raphnet_source', version: 'v99' });
    expect(result.name).toBe('raphnet_source');
    expect(result.link).toBe(dlLinks.raphnet_source);
    expect(result.version).toBe(allConfig.software.raphnet_source);
  }, 10000);
});
