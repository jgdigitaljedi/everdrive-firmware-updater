import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getSatiatorFw } from '../checkSatiatorFw.mjs';

describe('checkSatiatorFw', () => {
  it('should get the expected results from the Satiator call', async () => {
    const result = await getSatiatorFw();
    expect(result.name).toBe('satiator');
    expect(result.link).toBe(dlLinks.satiator);
    expect(result.version).toBe(allConfig.odes.satiator);
  }, 10000);
});
