import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getFenrirFw } from '../checkFenrirFw.mjs';

describe('checkFenrirFw', () => {
  it('should get the expected results from the Fenrir call', async () => {
    const result = await getFenrirFw();
    expect(result.name).toBe('fenrir');
    expect(result.link).toBe(dlLinks.fenrir);
    expect(result.version).toBe(allConfig.odes.fenrir);
  }, 10000);
});
