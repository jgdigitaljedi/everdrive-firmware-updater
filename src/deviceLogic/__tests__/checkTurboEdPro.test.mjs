import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getTurboEdProFw } from '../checkTurboEdProFw.mjs';

describe('checkTurboEdProFw', () => {
  it('should get the expected results from the Turbo EverDrive Pro call', async () => {
    const result = await getTurboEdProFw();
    expect(result.name).toBe('turboEdPro');
    expect(result.link).toBe(dlLinks.turboEdPro);
    expect(result.version).toBe(allConfig.everDrives.turboEdPro);
  }, 10000);
});
