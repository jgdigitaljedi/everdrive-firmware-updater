import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getAnaloguePocketFw } from '../checkAnaloguePocketFw.mjs';

describe('check64DriveFw', () => {
  it('should get the expected results for the Analogue Pocket', async () => {
    const result = await getAnaloguePocketFw();
    expect(result.name).toBe('pocket');
    expect(result.link).toBe(dlLinks.analoguePocket);
    expect(result.version).toBe(allConfig.devices.analoguePocket);
  }, 10000);
});
