import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getGcloaderFw } from '../checkGcloaderFw.mjs';

describe('checkGcloaderFw', () => {
  it('should get the expected results from the GCLoader call', async () => {
    const result = await getGcloaderFw();
    expect(result.name).toBe('gcloader');
    expect(result.link).toBe(dlLinks.gcloader);
    expect(result.version).toBe(allConfig.odes.gcloader);
  }, 10000);
});
