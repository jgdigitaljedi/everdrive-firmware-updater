import { get64DriveFw } from '../check64DriveFw.mjs';
import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };

describe('check64DriveFw', () => {
  it('should get the expected results from the 64Drive call (hw2)', async () => {
    const which = {
      name: 'hw2_64Drive',
      version: 'v99'
    };
    const result = await get64DriveFw(which);
    expect(result.name).toBe(which);
    expect(result.link).toBe(dlLinks.hw2_64Drive);
    expect(result.version).toBe(allConfig.flashCarts.hw2_64Drive);
  }, 20000);

  it('should get the expected results from the 64Drive call (hw1)', async () => {
    const which = {
      name: 'hw1_64Drive',
      version: 'v99'
    };
    const result = await get64DriveFw(which);
    expect(result.name).toBe(which);
    expect(result.link).toBe(dlLinks.hw1_64Drive);
    expect(result.version).toBe(allConfig.flashCarts.hw1_64Drive);
  }, 20000);
});
