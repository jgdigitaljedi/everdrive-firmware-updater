import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getRgbBlasterFw } from '../checkRgbBlasterFw.mjs';

describe('checkRgbBlasterFw', () => {
  it('should get the expected results from the RGB Blaster call', async () => {
    const result = await getRgbBlasterFw();
    expect(result.name).toBe('rgbBlaster');
    expect(result.link).toBe(dlLinks.rgbBlaster);
    expect(result.version).toBe(allConfig.devices.rgbBlaster);
  }, 10000);
});
