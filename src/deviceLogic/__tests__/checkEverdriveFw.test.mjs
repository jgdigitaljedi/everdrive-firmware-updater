import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import testConfig from '../../../testConfigs/testConfig.json' assert { type: 'json' };
import { getFirmwareVersion } from '../checkEverdriveFirmware.mjs';

describe('checkEverdriveFw', () => {
  const eds = Object.keys(testConfig.everDrives)
    .filter((key) => key !== 'turboEdPro')
    .map((key) => {
      return {
        name: key,
        version: testConfig.everDrives[key]
      };
    });
  test.each(eds)(
    'should get the expected results from the %p.name calls',
    async (ed) => {
      const result = await getFirmwareVersion(ed);
      expect(result.name).toBe(ed.name);
      expect(result.link).toBe(dlLinks[ed.name]);
      expect(result.version).toBe(allConfig.everDrives[ed.name]);
    },
    10000
  );
});
