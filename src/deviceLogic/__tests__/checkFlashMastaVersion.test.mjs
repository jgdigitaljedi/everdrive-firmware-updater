import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getFlashMastaVersion } from '../checkFlashMastaVersion.mjs';

describe('checkFlashMastaVersion', () => {
  const fms = [
    { name: 'flashmasta_mac', version: 'v99' },
    { name: 'flashmasta_linux', version: 'v99' },
    { name: 'flashmasta_windows', version: 'v99' }
  ];
  test.each(fms)('should get the expected results from the %p.name calls', async (fm) => {
    const result = await getFlashMastaVersion(fm);
    expect(result.name).toBe(fm.name);
    expect(result.link).toBe(dlLinks[fm.name]);
    expect(result.version).toBe(allConfig.software[fm.name]);
  });
});
