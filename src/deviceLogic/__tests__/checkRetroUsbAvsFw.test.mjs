import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getRetroUsbAvsFw } from '../checkRetroUsbAvsFw.mjs';

describe('checkRetroUsbAvsFw', () => {
  it('should get the expected results from the RetroSUB AVS fw call', async () => {
    const result = await getRetroUsbAvsFw();
    expect(result.name).toBe('retroUsbAvs');
    expect(result.link).toBe(dlLinks.retroUsbAvs);
    expect(result.version).toBe(allConfig.devices.retroUsbAvs);
  }, 10000);
});
