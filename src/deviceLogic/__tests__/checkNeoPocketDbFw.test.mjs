import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getNeoPocketGdFw } from '../checkNeoPocketGdFw.mjs';

describe('checkNeoPocketGdFw', () => {
  it('should get the expected results from the Neo Pocket GD call', async () => {
    const result = await getNeoPocketGdFw();
    expect(result.name).toBe('neoPocketGD');
    expect(result.link).toBe(dlLinks.neoPocketGD);
    expect(result.version).toBe(allConfig.flashCarts.neoPocketGD);
  }, 10000);
});
