import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getPsKai } from '../checkPsKaiVersion.mjs';

describe('checkPsKaiVersion', () => {
  it('should get the expected results from the Pseudo Saturn Kai call', async () => {
    const result = await getPsKai('psKai');
    expect(result.name).toBe('psKai');
    expect(result.link).toBe(dlLinks.psKai);
    expect(result.version).toBe(allConfig.devices.psKai);
  }, 10000);

  it('should get the expected results from the Pseudo Saturn Kai Menu call', async () => {
    const result = await getPsKai('psKaiMenu');
    expect(result.name).toBe('psKaiMenu');
    expect(result.link).toBe(dlLinks.psKaiMenu);
    expect(result.version).toBe(allConfig.devices.psKaiMenu);
  }, 10000);
});
