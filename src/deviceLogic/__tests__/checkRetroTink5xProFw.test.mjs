import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getRetroTink5xProFw } from '../checkRetroTink5xProFw.mjs';

describe('checkRetroTink5xProsFw', () => {
  it('should get the expected results from the RetroTink 5x Pro fw call', async () => {
    const result = await getRetroTink5xProFw();
    expect(result.name).toBe('retroTink5xPro');
    expect(result.link).toBe(dlLinks.retroTink5xPro);
    expect(result.version).toBe(allConfig.devices.retroTink5xPro);
  }, 10000);
});
