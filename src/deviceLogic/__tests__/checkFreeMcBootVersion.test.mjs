import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getFreeMcBootUpdates } from '../checkFreeMcBootVersion.mjs';

describe('checkFreeMcBootVersion', () => {
  it('should get the expected results from the Free McBoot call', async () => {
    const result = await getFreeMcBootUpdates();
    expect(result.name).toBe('freemcboot');
    expect(result.link).toBe(dlLinks.freemcboot);
    expect(result.version).toBe(allConfig.software.freemcboot);
  }, 10000);
});
