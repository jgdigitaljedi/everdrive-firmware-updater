import allConfig from '../../../testConfigs/allConfig.json' assert { type: 'json' };
import dlLinks from '../../../data/dlLinkList.json' assert { type: 'json' };
import { getRetroUsbScroeboardVersion } from '../checkRetroUsbScoreboardVersion.mjs';

describe('checkRetroUsbScoreboard', () => {
  const sbs = [
    { name: 'retroUsbSb_mac', version: 'v99' },
    { name: 'retroUsbSb_win', version: 'v99' }
  ];
  test.each(sbs)(
    'should get the expected results from the %p.name calls',
    async (sb) => {
      const result = await getRetroUsbScroeboardVersion(sb);
      expect(result.name).toBe(sb.name);
      expect(result.link).toBe(dlLinks[sb.name]);
      expect(result.version).toBe(allConfig.software[sb.name]);
    },
    10000
  );
});
