import chalk from 'chalk';
import config from '../../config.json' assert { type: 'json' };
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

const matchVersionNum = (ver) => {
  return ver.match(/v[0-9][0-9]/);
};

export async function getRgbBlasterFw() {
  try {
    const $ = await scrapeSite(otherFwUrls.rgbBlaster);
    const parent = $('pre');
    const pre = Array.from(parent)[0];
    const links = $(pre).find('a');
    const fwLinks = Array.from(links).filter((link) => $(link).text().includes('.zip'));
    const sorted = fwLinks.sort((a, b) => {
      const aVersion = $(a)
        .text()
        .match(/[0-9][0-9]/)[0];
      const bVersion = $(b)
        .text()
        .match(/[0-9][0-9]/)[0];
      if (parseInt(aVersion) > parseInt(bVersion)) {
        return -1;
      }
      if (parseInt(aVersion) < parseInt(bVersion)) {
        return 1;
      }
      return 0;
    });
    const latestFw = Array.from(sorted)[0];

    const latest = matchVersionNum($(latestFw).text())[0];
    if (config.devices.rgbBlaster !== latest) {
      const link = `${otherFwUrls.rgbBlaster}rgb-blaster-fw-${latest}.zip`;
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} RGB BLASTER FIRMWARE VERSION UPDATE AVAILABLE (${latest}): ${link}`
        )
      );
      await updateDlLinkList('rgbBlaster', link);
      return { name: 'rgbBlaster', link, version: latest };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} Your RGB Blaster firmware is currently up to date.`)
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
