import chalk from 'chalk';
import config from '../../config.json' assert { type: 'json' };
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function getAnaloguePocketFw() {
  try {
    const $ = await scrapeSite(otherFwUrls.pocket);
    const latestLinkButton = $('a[class*=rounded-full]');
    const link = $(latestLinkButton).attr('href');
    const linkSplit = link.split('/');
    const linkLast = linkSplit[linkSplit.length - 1];
    const linkVerNum = linkLast.replace('pocket_firmware_', '').replace('.bin', '');
    const latest = linkVerNum.replace('_', '.');

    if (config.devices.analoguePocket !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} ANALOGUE POCKET FIRMWARE VERSION UPDATE AVAILABLE (${latest}): ${link}`
        )
      );
      await updateDlLinkList('analoguePocket', link);
      return { name: 'pocket', link, version: latest };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} Your Analogue Pocket firmware is currently up to date.`)
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
