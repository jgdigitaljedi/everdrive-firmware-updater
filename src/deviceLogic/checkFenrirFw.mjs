import chalk from 'chalk';
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import config from '../../config.json' assert { type: 'json' };
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function getFenrirFw() {
  try {
    const $ = await scrapeSite(otherFwUrls.fenrir);
    const parentArr = $('.table-2');
    const parent = Array.from(parentArr)[0];
    const versionArr = $(parent).find('small');
    const latestEle = Array.from(versionArr)[0];
    const latest = $(latestEle).text().trim();
    const linkEle = $(parent).find('a');
    const linkLast = $(Array.from(linkEle)[1]).attr('href');
    const dlLink = `${otherFwUrls.fenrirDl}${linkLast}`;

    if (config.odes.fenrir !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} FENRIR VERSION UPDATE AVAILABLE (${latest}): ${otherFwUrls.fenrirDl}${dlLink}`
        )
      );
      await updateDlLinkList('fenrir', dlLink);
      return { name: 'fenrir', link: dlLink, version: latest };
    } else {
      console.log(chalk.cyan(`${statusEmo.check} Fenrir firmware is currently up to date.`));
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
