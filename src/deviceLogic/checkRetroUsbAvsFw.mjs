import chalk from 'chalk';
import config from '../../config.json' assert { type: 'json' };
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import { matchVVersion } from '../../util/versionHelpers.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';
import puppeteer from 'puppeteer';

export async function getRetroUsbAvsFw() {
  const browser = await puppeteer.launch();
  try {
    const page = await browser.newPage();
    await page.goto(otherFwUrls.retroUsbAvs);
    await page.setViewport({ width: 1080, height: 1024 });
    await page.waitForSelector('.grid__item', { timeout: 10_000 });
    const latestFw = await page.$$eval('a', (links) =>
      links
        .map((link) => ({ href: link.href, text: link.innerText }))
        .filter((link) => link.text.includes('AVS v'))
    );
    const latest = matchVVersion(latestFw[0].text);
    if (config.devices.retroUsbAvs !== latest) {
      const link = latestFw[0].href;
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} RETROUSB AVS FIRMWARE UPDATE AVAILABLE (${latest}): ${link}`
        )
      );
      await updateDlLinkList('retroUsbAvs', link);
      await browser.close();
      return { name: 'retroUsbAvs', link, version: latest };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} Your RetroUSB AVS firmware is currently up to date.`)
      );
      await browser.close();
      return null;
    }
  } catch (error) {
    await browser.close();
    await puppeteer.close();
    return Promise.reject({ error });
  }
}
