import chalk from 'chalk';
import config from '../../config.json' assert { type: 'json' };
import { getGithubData } from '../../util/githubHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function getOplUpdate() {
  try {
    const { latest, dlLink, assets } = await getGithubData('ps2homebrew', 'Open-PS2-Loader', true);
    const allDls = assets
      .filter((item) => item.name.includes('.7z'))
      .map((item) => {
        return {
          name: item.name,
          link: item.browser_download_url
        };
      });

    if (config.software.opl !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} OPEN PS2 LOADER SOFTWARE UPDATE AVAILABLE (${latest}): ${dlLink}`
        )
      );
      await updateDlLinkList(
        'opl',
        allDls.map((a) => a.link).filter((a) => a.indexOf('OPNPS2LD.7z') >= 0)[0]
      );
      return { name: 'opl', link: dlLink, version: latest, dlList: allDls };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} Open PS2 Loader software is currently up to date.`)
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
