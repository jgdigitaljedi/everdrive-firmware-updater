import chalk from 'chalk';
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import config from '../../config.json' assert { type: 'json' };
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function getSatiatorFw() {
  try {
    const $ = await scrapeSite(otherFwUrls.satiator);
    const linkArr = Array.from($('a'));
    const latestEle = linkArr[linkArr.length - 1];
    const latest = $(latestEle).text().trim().split('.')[0];
    const linkEle = $(latestEle).text();
    const dlLink = `${otherFwUrls.satiatorDl}${linkEle}`;

    if (config.odes.satiator !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} SATIATOR VERSION UPDATE AVAILABLE (${latest}): ${otherFwUrls.satiatorDl}${dlLink}`
        )
      );
      await updateDlLinkList('satiator', dlLink);
      return { name: 'satiator', link: dlLink, version: latest };
    } else {
      console.log(chalk.cyan(`${statusEmo.check} Satiator firmware is currently up to date.`));
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
