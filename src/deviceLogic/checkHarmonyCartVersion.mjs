import chalk from 'chalk';
import { getGithubData } from '../../util/githubHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

const osToAsset = {
  harmonycart_windows: {
    file: 'win32.exe',
    messageName: 'Windows',
    ext: '.exe'
  },
  harmonycart_mac: {
    file: 'macosx.dmg',
    messageName: 'Mac',
    ext: '.dmg'
  },
  harmonycart_linux: {
    file: 'amd64.deb',
    messageName: 'Linux (.deb)',
    ext: '.deb'
  },
  harmonycart_source: {
    file: 'src.tar.gz',
    messageName: 'Source',
    ext: '.tar.gz'
  }
};

export async function getHarmonyCartVersion(which) {
  try {
    const { latest, assets } = await getGithubData('sa666666', 'harmonycart', true);
    const osProps = osToAsset[which.name];
    const dlForOs = assets.filter((item) => item.name.includes(osProps.file))[0]
      .browser_download_url;

    if (which.version !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${
            statusEmo.up
          } HARMONY CART ${osProps.messageName.toUpperCase()} SOFTWARE UPDATE AVAILABLE (${latest}): ${dlForOs}`
        )
      );
      await updateDlLinkList(which.name, dlForOs);
      return { name: which.name, link: dlForOs, version: latest, ext: osProps.ext };
    } else {
      console.log(
        chalk.cyan(
          `${statusEmo.check} Harmony Cart ${osProps.messageName} software is currently up to date.`
        )
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
