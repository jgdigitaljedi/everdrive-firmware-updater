import chalk from 'chalk';
import config from '../../config.json' assert { type: 'json' };
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function getAnalogueDuoFw() {
  try {
    const $ = await scrapeSite(otherFwUrls.duo);
    const className = $('[class*=firmwares_version__]').attr('class');
    const container = $(`.${className} > p`);
    const filtered = Array.from(container).filter((item, index) => index === 0)[0];
    const latest = $(filtered).text();
    if (config.devices.analogueDuo !== latest) {
      const link = $('a[class*=new_button__]').attr('href');
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} ANALOGUE DUO FIRMWARE VERSION UPDATE AVAILABLE (${latest}): ${link}`
        )
      );
      await updateDlLinkList('analogueDuo', link);
      return { name: 'pocket', link, version: latest };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} Your Analogue Duo firmware is currently up to date.`)
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
