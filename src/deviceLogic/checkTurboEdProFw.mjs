import chalk from 'chalk';
import config from '../../config.json' assert { type: 'json' };
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import { matchVVersion } from '../../util/versionHelpers.mjs';
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function getTurboEdProFw() {
  try {
    const $ = await scrapeSite(otherFwUrls.turboEdPro);
    const parent = $('pre');
    const pre = Array.from(parent)[0];
    const links = $(pre).find('a');
    const fwLinks = Array.from(links).filter((link) => $(link).text().includes('.efu'));
    const sorted = fwLinks.sort((a, b) => {
      const aVersion = $(a)
        .text()
        .match(/[0-9].[0-9][0-9]/);
      const bVersion = $(b)
        .text()
        .match(/[0-9].[0-9][0-9]/);
      if (aVersion > bVersion) {
        return -1;
      }
      if (aVersion < bVersion) {
        return 1;
      }
      return 0;
    });
    const latestFw = sorted[0];
    const latest = matchVVersion($(latestFw).text());
    if (config.everDrives.turboEdPro !== latest) {
      const link = `${otherFwUrls.turboEdPro}${latest}`;
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} TURBO EVERDRIVE PRO FIRMWARE VERSION UPDATE AVAILABLE (${latest}): ${link}`
        )
      );
      await updateDlLinkList('turboEdPro', link);
      return { name: 'turboEdPro', link, version: latest };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} Your Turbo EverDrive Pro firmware is currently up to date.`)
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
