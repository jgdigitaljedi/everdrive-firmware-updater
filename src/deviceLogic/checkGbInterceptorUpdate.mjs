import chalk from 'chalk';
import config from '../../config.json' assert { type: 'json' };
import { getGithubData } from '../../util/githubHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function getGbInterceptorUpdate() {
  try {
    const { latest, dlLink, assets } = await getGithubData('Staacks', 'gbinterceptor', true);
    const dlList = assets.filter((item) => item.browser_download_url.includes('gbinterceptor'));

    if (config.devices.gbinterceptor !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} GB INTERCEPTOR FIRMWARE UPDATE AVAILABLE (${latest}): ${dlLink}`
        )
      );
      await updateDlLinkList('gbinterceptor', dlLink);
      return { name: 'gbinterceptor', link: dlLink, version: latest, dlList };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} GB Interceptor firmware is currently up to date.`)
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
