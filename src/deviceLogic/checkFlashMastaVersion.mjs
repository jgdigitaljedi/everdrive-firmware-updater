import chalk from 'chalk';
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import config from '../../config.json' assert { type: 'json' };
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

const osToLink = ($, which) => {
  switch (which) {
    case 'WINDOWS':
      return Array.from($('a[href*="Win"]'))[1];
    case 'MAC':
      return Array.from($('a[href*="Mac"]'))[0];
    case 'LINUX':
      return Array.from($('a[href*="Linux"]'))[0];
  }
};

export async function getFlashMastaVersion(which) {
  try {
    const os = which.name.split('_')[1].toUpperCase();
    const $ = await scrapeSite(otherFwUrls.flashmasta);

    const link = osToLink($, os);
    const latest = $(link).text().trim();
    const dlLink = $(link).attr('href');

    if (config.software[which.name] !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} FLASHMASTA ${os} SOFTWARE VERSION UPDATE AVAILABLE (${latest}): ${dlLink}`
        )
      );
      await updateDlLinkList(which.name, dlLink);
      return { name: which.name, link: dlLink, version: latest };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} FlashMasta ${os} software is currently up to date.`)
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
