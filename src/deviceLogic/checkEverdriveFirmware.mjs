import chalk from 'chalk';
import { edFwUrls } from '../../util/constants/everDriveConstants.mjs';
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function getFirmwareVersion(which) {
  try {
    const configData = edFwUrls[which.name];
    const url = configData.url;
    const $ = await scrapeSite(url);
    const parent = $('table');
    const table = Array.from(parent)[configData.tableNum];
    const container = Array.from($(table).find('tr'))[1];
    const cells = Array.from($(container).find('td'));
    const filtered = cells[0];
    const latest = $(filtered).text();
    if (which.version !== latest) {
      const link = $(cells[configData.isLegacy ? 2 : 3])
        .find('a')
        .attr('href');
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} ${which.name} EVERDRIVE VERSION UPDATE AVAILABLE (${latest}): ${link}`
        )
      );
      await updateDlLinkList(which.name, link);
      return { name: which.name, link, version: latest };
    } else {
      console.log(
        chalk.cyan(
          `${statusEmo.check} Your ${which.name} EverDrive firmware is currently up to date.`
        )
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
