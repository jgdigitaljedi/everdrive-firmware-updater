import chalk from 'chalk';
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import config from '../../config.json' assert { type: 'json' };
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function getFreeMcBootUpdates() {
  try {
    const $ = await scrapeSite(otherFwUrls.freemcboot);
    const linkArr = Array.from($('a.btn.btn-outline'));
    const latestEle = linkArr[0];
    const latest = $(latestEle).text().trim();
    const dlLink = $(latestEle).attr('href');

    if (config.software.freemcboot !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} FREE MCBOOT VERSION UPDATE AVAILABLE (${latest}): ${dlLink}`
        )
      );
      await updateDlLinkList('freemcboot', dlLink);
      return { name: 'freemcboot', link: dlLink, version: latest };
    } else {
      console.log(chalk.cyan(`${statusEmo.check} Free McBoot version is currently up to date.`));
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
