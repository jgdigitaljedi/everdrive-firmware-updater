import chalk from 'chalk';
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function get64DriveFw(which) {
  try {
    const $ = await scrapeSite(otherFwUrls.drive64);
    const parentContainer = $('.table-wrapper');
    const parent = Array.from(parentContainer)[1];
    const tbody = $(parent).find('tbody');
    const container = Array.from($(tbody).find('tr'))[which.name === 'hw2_64Drive' ? 1 : 2];
    const cells = Array.from($(container).find('td'));
    const filtered = cells[3];
    const latest = $(filtered).text();
    const whichSplit = which.name.split('_');
    if (which.version !== latest) {
      const link = $(cells[4]).find('a').attr('href');
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} 64Drive (${whichSplit[0]}) VERSION UPDATE AVAILABLE (${latest}): ${otherFwUrls.drive64Dl}${link}`
        )
      );
      await updateDlLinkList(which.name, `${otherFwUrls.drive64Dl}${link}`);
      return { name: which, link: `${otherFwUrls.drive64Dl}${link}`, version: latest };
    } else {
      console.log(
        chalk.cyan(
          `${statusEmo.check} Your 64Drive (${whichSplit[0]}) firmware is currently up to date.`
        )
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
