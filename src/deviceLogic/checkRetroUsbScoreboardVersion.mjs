import chalk from 'chalk';
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import config from '../../config.json' assert { type: 'json' };
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { matchVVersion } from '../../util/versionHelpers.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';
import puppeteer from 'puppeteer';

const osToLink = (links, which) => {
  switch (which) {
    case 'WIN':
      return links.find((link) => link.text.toLowerCase().includes('win'));
    case 'MAC':
      return links.find((link) => link.text.toLowerCase().includes('mac'));
  }
};

export async function getRetroUsbScroeboardVersion(which) {
  const browser = await puppeteer.launch();
  try {
    const os = which.name.split('_')[1].toUpperCase();
    const page = await browser.newPage();
    await page.goto(otherFwUrls.retroUsbAvs);
    await page.setViewport({ width: 1080, height: 1024 });
    await page.waitForSelector('.grid__item', { timeout: 10_000 });
    const latestScorboard = await page.$$eval('a', (links) =>
      links
        .map((link) => ({ href: link.href, text: link.innerText }))
        .filter((link) => link.text.toLowerCase().includes('scoreboard'))
    );
    const link = osToLink(latestScorboard, os);
    const latest = matchVVersion(link.text);
    const dlLink = link.href;

    if (config.software[which.name] !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} RETROUSB SCOREBOARD ${os} SOFTWARE VERSION UPDATE AVAILABLE (${latest}): ${dlLink}`
        )
      );
      await updateDlLinkList(which.name, dlLink);
      await browser.close();
      return { name: which.name, link: dlLink, version: latest };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} RetroUSB Scoreboard ${os} software is currently up to date.`)
      );
      await browser.close();
      return null;
    }
  } catch (error) {
    await browser.close();
    await puppeteer.close();
    return Promise.reject({ error });
  }
}
