import chalk from 'chalk';
import { getGithubData } from '../../util/githubHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

const versionToAsset = {
  r77FwNg_sdcard: {
    ext: '.img',
    message: 'SD card',
    file: 'sdcard.img'
  },
  r77FwNg_uImage: {
    ext: '',
    message: 'uImage',
    file: 'uImage'
  }
};

export async function getR77FwNgVersion(which) {
  try {
    const { latest, assets } = await getGithubData('DirtyHairy', 'r77-firmware-ng', true);
    const versionProps = versionToAsset[which.name];
    const dlForVersion = assets.filter((item) => item.name.includes(versionProps.file))[0]
      .browser_download_url;

    if (which.version !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${
            statusEmo.up
          } RETRON 77 FW NG ${versionProps.message.toUpperCase()} UPDATE AVAILABLE (${latest}): ${dlForVersion}`
        )
      );
      await updateDlLinkList(which.name, dlForVersion);
      return { name: which.name, link: dlForVersion, version: latest, ext: versionProps.ext };
    } else {
      console.log(
        chalk.cyan(
          `${statusEmo.check} Retron 77 Firmware ${versionProps.message} is currently up to date.`
        )
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
