import chalk from 'chalk';
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import config from '../../config.json' assert { type: 'json' };
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

const swOrMenu = {
  psKai: {
    selector: 'a[href*="pskai_release"]',
    itemName: 'Pseudo Saturn Kai'
  },
  psKaiMenu: {
    selector: 'a[href*="saturn_ode_menu"]',
    itemName: 'Pseudo Saturn Kai Menu'
  }
};

export async function getPsKai(which) {
  try {
    const $ = await scrapeSite(otherFwUrls.psKai);
    const linkEle = Array.from($(swOrMenu[which].selector))[0];
    const linkText = $(linkEle).text();
    const textSplit = linkText.split('_');
    const lastSplit = textSplit[textSplit.length - 1].split('.');
    const latest = lastSplit[0];
    if (config[which] !== latest) {
      const link = $(linkEle).attr('href').split('/')[1];
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} ${swOrMenu[
            which
          ].itemName.toUpperCase()} VERSION UPDATE AVAILABLE (${latest}): ${
            otherFwUrls.psKai
          }${link}`
        )
      );
      await updateDlLinkList(which, `${otherFwUrls.psKai}${link}`);
      return { name: which, link: `${otherFwUrls.psKai}${link}`, version: latest };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} Your ${swOrMenu[which].itemName} is currently up to date.`)
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
