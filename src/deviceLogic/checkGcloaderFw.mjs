import chalk from 'chalk';
import config from '../../config.json' assert { type: 'json' };
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

/** Even though the logic here is mostly the same as the EverDrive logic, I'm keeping this in a separate file
 * in the event that the website changes formats a bit. This is a "gotcha" of web scraping sometimes.
 */

export async function getGcloaderFw() {
  try {
    const $ = await scrapeSite(otherFwUrls.gcloader);
    const parent = $('table');
    const table = Array.from(parent)[0];
    const container = Array.from($(table).find('tr'))[1];
    const cells = Array.from($(container).find('td'));
    const filtered = cells[0];
    const latest = $(filtered).text();
    if (config.odes.gcloader !== latest) {
      const link = $(cells[3]).find('a').attr('href');
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} GCLOADER VERSION UPDATE AVAILABLE (${latest}): ${link}`
        )
      );
      await updateDlLinkList('gcloader', link);
      return { name: 'gcloader', link, version: latest };
    } else {
      console.log(chalk.cyan(`${statusEmo.check} Your GCLoader firmware is currently up to date.`));
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
