import chalk from 'chalk';
import config from '../../config.json' assert { type: 'json' };
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function getRetroTink5xProFw() {
  try {
    const $ = await scrapeSite(otherFwUrls.retroTink5xPro);
    const dlLinks = $('a[href*=".zip"]');
    const fwLinks = Array.from(dlLinks).filter((link) => $(link).text() === 'Download');
    const latestFw = fwLinks[0];
    const versions = $('h2[id*="version-"]');
    const latestVersion = Array.from(versions)[0];
    const latest = $(latestVersion).text();
    if (config.devices.retroTink5xPro !== latest) {
      const link = $(latestFw).attr('href');
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} RETROTINK 5X PRO FIRMWARE UPDATE AVAILABLE (${latest}): ${link}`
        )
      );
      await updateDlLinkList('retroTink5xPro', link);
      return { name: 'retroTink5xPro', link, version: latest };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} Your RetroTink 5x Pro firmware is currently up to date.`)
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
