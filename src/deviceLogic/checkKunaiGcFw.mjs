import chalk from 'chalk';
import { getGithubData } from '../../util/githubHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import config from '../../config.json' assert { type: 'json' };
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function getKunaiGcFw() {
  try {
    const { latest, dlLink, assets } = await getGithubData('KunaiGC', 'KunaiGC', true);
    const dlList = assets.filter((item) => item.browser_download_url.includes('Kunai'));

    if (config.mods.kunaiGc !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} KUNAI GC FIRMWARE UPDATE AVAILABLE (${latest}): ${dlLink}`
        )
      );
      await updateDlLinkList('kunaiGc', dlLink);
      return { name: 'kunaiGc', link: dlLink, version: latest, dlList: dlList };
    } else {
      console.log(chalk.cyan(`${statusEmo.check} Kunai GC firmware is currently up to date.`));
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
