import chalk from 'chalk';
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import config from '../../config.json' assert { type: 'json' };
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function getR4SdhcFw() {
  try {
    const $ = await scrapeSite(otherFwUrls.r4sdhc);
    const table = $('table');
    const rows = $(table).find('tr');
    const cells = $(Array.from(rows)[0]).find('td');
    const link = $(Array.from(cells)[0]).find('a');
    const dlLink = $(link).attr('href');
    const urlSplit = dlLink.split('%');
    const versionSplit = urlSplit[0].split('-');
    const latest = versionSplit[1];

    if (config.flashCarts.r4sdhc !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} R4SDHC FIRMWARE UPDATE AVAILABLE (${latest}): ${dlLink}`
        )
      );
      await updateDlLinkList('r4sdhc', dlLink);
      return { name: 'r4sdhc', link: dlLink, version: latest };
    } else {
      console.log(chalk.cyan(`${statusEmo.check} R4SDHC firmware is currently up to date.`));
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
