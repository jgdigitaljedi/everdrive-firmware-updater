import chalk from 'chalk';
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import config from '../../config.json' assert { type: 'json' };
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

const whichIndex = (which) => {
  if (which === 'AUDIO') {
    return 'release_audio';
  }
  return 'release_non-audio';
};

const getDlLink = ($, language) => {
  if (language === 'en') {
    const links = Array.from($('a')).filter((ele) => $(ele).attr('href').indexOf('jp') === -1);
    return links[links.length - 1];
  }
  const links = $('a[href*="jp"]');
  return links[links.length - 1];
};

export async function getOsscFw(which) {
  try {
    const hdmiAudio = which.name.split('_')[1];
    const language = which.name.split('_')[2];
    const fullUrl = `${otherFwUrls.ossc}${whichIndex(hdmiAudio.toUpperCase())}`;
    const $ = await scrapeSite(fullUrl);
    const versionText = `${hdmiAudio}-${language}`;

    const link = getDlLink($, language);
    const version = $(link).text();
    const latest = version.replace('.bin', '');
    const dlLink = `${fullUrl}/${$(link).attr('href')}`;

    if (config.devices[which.name] !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${
            statusEmo.up
          } OSSC ${versionText.toUpperCase()} VERSION FIRMWARE VERSION UPDATE AVAILABLE (${latest}): ${dlLink}`
        )
      );
      await updateDlLinkList(which.name, dlLink);
      return { name: which.name, link: dlLink, version: latest };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} OSSC ${versionText} software is currently up to date.`)
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
