import chalk from 'chalk';
import config from '../../config.json' assert { type: 'json' };
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function getRetroTink4kFw() {
  try {
    const $ = await scrapeSite(otherFwUrls.retroTink4k);
    const fwLinks = $('a[href*=".zip"]');
    const latestFw = Array.from(fwLinks)[0];
    const versions = $('h2[id*="version-"]');
    const latestVersion = Array.from(versions)[0];
    const latest = $(latestVersion).text();
    if (config.devices.retroTink4k !== latest) {
      const link = $(latestFw).attr('href');
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} RETROTINK 4K FIRMWARE UPDATE AVAILABLE (${latest}): ${link}`
        )
      );
      await updateDlLinkList('retroTink4k', link);
      return { name: 'retroTink4k', link, version: latest };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} Your RetroTink 4K firmware is currently up to date.`)
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
