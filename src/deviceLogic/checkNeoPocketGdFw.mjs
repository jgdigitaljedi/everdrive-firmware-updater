import chalk from 'chalk';
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import config from '../../config.json' assert { type: 'json' };
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

export async function getNeoPocketGdFw() {
  try {
    const $ = await scrapeSite(otherFwUrls.neoPocketGD);
    const links = $('a');
    const dlLink = Array.from(links).filter((link) =>
      $(link).text().includes('GameDrive Firmware')
    )[0];
    const fwText = $(dlLink).text();
    const fwTextSplit = fwText.split(' ');
    const latest = fwTextSplit[fwTextSplit.length - 1];

    if (config.flashCarts.neoPocketGD !== latest) {
      const link = $(dlLink).attr('href');
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} NEO POCKET GAMEDRIVE VERSION UPDATE AVAILABLE (${latest}): ${link}`
        )
      );
      await updateDlLinkList('neoPocketGD', link);
      return { name: 'neoPocketGD', link, version: latest };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} Your Neo Pocket GameDrive firmware is currently up to date.`)
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}



