import chalk from 'chalk';
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

const raphnetVersionMap = {
  raphnet_win: '.exe',
  raphnet_source: '.tar.gz'
};

const getVersion = (str, fromEnd) => {
  const noExt = str.substring(0, str.length - fromEnd);
  const strSplit = noExt.split('-');
  return strSplit[strSplit.length - 1];
};

export async function getRaphnetVersion(which) {
  try {
    const fileExt = raphnetVersionMap[which.name];
    const $ = await scrapeSite(otherFwUrls.raphnetAdapterManager);
    const fwLinks = $(`a[href*="${fileExt}"]`);
    const latestFw = Array.from(fwLinks)[0];
    const latest = getVersion($(latestFw).text(), fileExt.length);
    if (which.version !== latest) {
      const linkEnd = $(latestFw).attr('href');
      const link = `${otherFwUrls.raphnetAdapterManager}${linkEnd}`;
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} RAPHNET ADAPTER MANAGER UPDATE AVAILABLE (${latest}): ${link}`
        )
      );
      await updateDlLinkList(which.name, link);
      return { name: which.name, link, version: latest, ext: fileExt };
    } else {
      console.log(
        chalk.cyan(
          `${statusEmo.check} Your Raphnet Adapter Manager software is currently up to date.`
        )
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
