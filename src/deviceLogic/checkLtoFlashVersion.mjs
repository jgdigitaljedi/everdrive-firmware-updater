import chalk from 'chalk';
import otherFwUrls from '../../util/constants/otherFwUrls.mjs';
import config from '../../config.json' assert { type: 'json' };
import { scrapeSite } from '../../util/scraperHelper.mjs';
import { updateDlLinkList } from '../../util/updateDlLinkList.mjs';
import { statusEmo } from '../../util/constants/statusEmojies.mjs';

const osToLink = ($, which) => {
  switch (which) {
    case 'WINDOWS':
      return Array.from($('a[href*="LTOFlash-Windows"]'))[0];
    case 'MAC':
      return Array.from($('a[href*="LTOFlash-Mac"]'))[0];
    case 'SOURCE':
      return Array.from($('a[href*="LTOFlash.source"]'))[0];
  }
};

const getVersion = (latest) => {
  const latestSplit = latest.split('-');
  const containsVersion = latestSplit[latestSplit.length - 1];
  const splitExtension = containsVersion.split('.');
  splitExtension.splice(-1);
  return splitExtension.join('.');
};

export async function getLtoFlashVersion(which) {
  try {
    const os = which.name.split('_')[1].toUpperCase();
    const $ = await scrapeSite(otherFwUrls.ltoflash);

    const link = osToLink($, os);
    const dlLink = $(link).attr('href');
    const latest = getVersion(dlLink);

    if (config.software[which.name] !== latest) {
      console.log(
        chalk.yellow.bold(
          `\n${statusEmo.up} LTO FLASH ${os} SOFTWARE VERSION UPDATE AVAILABLE (${latest}): ${otherFwUrls.ltoflash}${dlLink}`
        )
      );
      await updateDlLinkList(which.name, `${otherFwUrls.ltoflash}${dlLink}`);
      return { name: which.name, link: `${otherFwUrls.ltoflash}${dlLink}`, version: latest };
    } else {
      console.log(
        chalk.cyan(`${statusEmo.check} LTO Flash ${os} software is currently up to date.`)
      );
      return null;
    }
  } catch (error) {
    return Promise.reject({ error });
  }
}
