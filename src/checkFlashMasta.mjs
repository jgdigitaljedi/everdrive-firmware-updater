import chalk from 'chalk';
import config from '../config.json' assert { type: 'json' };
import { getFlashMastaVersion } from './deviceLogic/checkFlashMastaVersion.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkFlashMasta = async () => {
  const fmsNames = ['flashmasta_mac', 'flashmasta_linux', 'flashmasta_windows'];
  const fms = fmsNames
    .map((key) => ({ name: key, version: config.software[key] || undefined }))
    .filter((i) => !!i.version);
  for (let i = 0; i < fms.length; i++) {
    const current = fms[i];
    try {
      const newFw = await getFlashMastaVersion(current);
      if (newFw && !newFw.error) {
        await writeUpdatedConfig(current.name, 'software', newFw.version);
        await downloadFw(
          newFw,
          `../downloads/software/${current.name}_${newFw.version}.zip`,
          `${statusEmo.fire} Error downloading software for ${current.name}`
        );
        console.log(
          chalk.magenta(
            `${statusEmo.game} Downloaded FlashMasta updater software: ${current.name}!\n`
          )
        );
      }
    } catch (error) {
      console.log(
        chalk.red.bold(`${statusEmo.fire} Error fetching update info for ${current.name}`)
      );
    }
  }
};

export default checkFlashMasta;
