import genericGhDevices from '../util/constants/genericGhDevices.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkTeos = async () => {
  const device = genericGhDevices.teos;
  await genericGhUpdate(device);
};

export default checkTeos;
