import genericGhDevices from '../util/constants/genericGhDevices.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkPicoBoot = async () => {
  const device = genericGhDevices.picoboot;
  await genericGhUpdate(device);
};

export default checkPicoBoot;
