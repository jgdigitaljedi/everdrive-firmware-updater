import chalk from 'chalk';
import config from '../config.json' assert { type: 'json' };
import { getRetroUsbScroeboardVersion } from './deviceLogic/checkRetroUsbScoreboardVersion.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkRetroUsbScorecard = async () => {
  const scNames = ['retroUsbSb_win', 'retroUsbSb_mac'];
  const sc = scNames
    .map((key) => ({ name: key, version: config.software[key] || undefined }))
    .filter((i) => !!i.version);
  for (let i = 0; i < sc.length; i++) {
    const current = sc[i];
    try {
      const newFw = await getRetroUsbScroeboardVersion(current);
      if (newFw && !newFw.error) {
        await writeUpdatedConfig(current.name, 'software', newFw.version);
        await downloadFw(
          newFw,
          `../downloads/software/${current.name}_${newFw.version}.zip`,
          `${statusEmo.fire} Error downloading software for ${current.name}`
        );
        console.log(
          chalk.magenta(
            `${statusEmo.game} Downloaded RetroUSB Scorecard software: ${current.name}!\n`
          )
        );
      }
    } catch (error) {
      console.log(
        chalk.red.bold(`${statusEmo.fire} Error fetching update info for ${current.name}`)
      );
    }
  }
};

export default checkRetroUsbScorecard;
