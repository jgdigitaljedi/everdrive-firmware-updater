import chalk from 'chalk';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { getPsKai } from './deviceLogic/checkPsKaiVersion.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const swOrMenu = {
  psKai: {
    fileName: 'PseudoSaturnKai',
    itemName: 'Pseudo Saturn Kai'
  },
  psKaiMenu: {
    fileName: 'PseudoSaturnKaiMenu',
    itemName: 'Pseudo Saturn Kai Menu'
  }
};

const checkPsKai = async (which) => {
  try {
    const newFw = await getPsKai(which);
    if (newFw && !newFw.error) {
      await writeUpdatedConfig(which, 'devices', newFw.version);
      await downloadFw(
        newFw,
        `../downloads/devices/${swOrMenu[which].fileName}_${newFw.version}.7z`,
        `${statusEmo.fire} Error downloading ${swOrMenu[which].itemName}`
      );
      console.log(chalk.magenta(`${statusEmo.game} Downloaded ${swOrMenu[which].itemName}!\n`));
    }
  } catch (error) {
    console.log(
      chalk.red.bold(`${statusEmo.fire} Error fetching update info for Pseudo Saturn Kai`)
    );
  }
};

export default checkPsKai;
