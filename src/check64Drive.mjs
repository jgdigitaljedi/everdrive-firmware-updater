import chalk from 'chalk';
import config from '../config.json' assert { type: 'json' };
import { get64DriveFw } from './deviceLogic/check64DriveFw.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const check64DriveFw = async () => {
  const drives = ['hw2_64Drive', 'hw1_64Drive']
    .map((item) => {
      if (config.flashCarts[item]) {
        return {
          name: item,
          version: config.flashCarts[item]
        };
      }
      return null;
    })
    .filter((i) => !!i);
  for (let i = 0; i < drives.length; i++) {
    try {
      const current = drives[i];
      const newFw = await get64DriveFw(current);
      if (newFw && !newFw.error) {
        await writeUpdatedConfig(drives.name, 'flashCarts', newFw.version);
        const downloaded = await downloadFw(
          newFw,
          `../downloads/flashcarts/${current.name}_${newFw.version}.rpk`,
          `${statusEmo.fire} Error downloading firmware for ${current.name}`
        );
        if (downloaded) {
          console.log(
            chalk.magenta(`${statusEmo.game} Downloaded firmware for your ${drives[i].name}!\n`)
          );
        }
      }
    } catch (error) {
      console.log(
        chalk.red.bold(
          `${statusEmo.fire} Error fetching update info for ${drives[i].name}: ${error}`
        )
      );
    }
  }
};

export default check64DriveFw;
