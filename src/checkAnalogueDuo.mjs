import chalk from 'chalk';
import { getAnalogueDuoFw } from './deviceLogic/checkAnalogueDuoFw.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkAnalogueDuo = async () => {
  try {
    const newFw = await getAnalogueDuoFw();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('analogueDuo', 'devices', newFw.version);
      await downloadFw(
        newFw,
        `../downloads/devices/analogueDuo_${newFw.version}.zip`,
        `${statusEmo.fire} Error downloading firmware for Analogue Duo`
      );
      console.log(chalk.magenta(`${statusEmo.game} Downloaded firmware for you Analogue Duo!\n`));
    }
  } catch (error) {
    console.log(chalk.red.bold(`${statusEmo.fire} Error fetching update info for Analogue Duo`));
  }
};

export default checkAnalogueDuo;
