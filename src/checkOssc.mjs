import chalk from 'chalk';
import config from '../config.json' assert { type: 'json' };
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { getOsscFw } from './deviceLogic/checkOsscFw.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkOssc = async () => {
  const osscNames = ['ossc_audio_en', 'ossc_audio_jp', 'ossc_noAudio_en', 'ossc_noAudio_jp'];
  const osscs = osscNames
    .map((key) => ({ name: key, version: config.devices[key] || undefined }))
    .filter((i) => !!i.version);
  for (let i = 0; i < osscs.length; i++) {
    const current = osscs[i];
    try {
      const newFw = await getOsscFw(current);
      if (newFw && !newFw.error) {
        await writeUpdatedConfig(current.name, 'devices', newFw.version);
        await downloadFw(
          newFw,
          `../downloads/devices/${current.name}_${newFw.version}.bin`,
          `${statusEmo.fire} Error downloading software for ${current.name}`
        );
        console.log(
          chalk.magenta(`${statusEmo.game} Downloaded OSSC firmware: ${current.name}!\n`)
        );
      }
    } catch (error) {
      console.log(
        chalk.red.bold(`${statusEmo.fire} Error fetching update info for ${current.name}`)
      );
    }
  }
};

export default checkOssc;
