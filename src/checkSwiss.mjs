import genericGhDevices from '../util/constants/genericGhDevices.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkSwiss = async () => {
  const device = genericGhDevices.swiss;
  await genericGhUpdate(device);
};

export default checkSwiss;
