import chalk from 'chalk';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { getKunaiGcFw } from './deviceLogic/checkKunaiGcFw.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

// it occurred to me not to rename binary files
const dlName = (file) => {
  if (file.toLowerCase().includes('.bin')) {
    return `../downloads/mods/${file}`;
  }
  return `../downloads/mods/KunaiGC_${file}`;
};

const checkKunaiGc = async () => {
  try {
    const newFw = await getKunaiGcFw();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('kunaiGc', 'mods', newFw.version);
      const dlList = newFw.dlList;
      for (let i = 0; i < dlList.length; i++) {
        await downloadFw(
          newFw,
          dlName(dlList[i].name),
          `${statusEmo.fire} Error downloading latest version of Kunai GC firmware`
        );
      }
      console.log(
        chalk.magenta(`${statusEmo.game} Downloaded latest version of Kunai GC firmware!\n`)
      );
    }
  } catch (error) {
    console.log(
      chalk.red.bold(`${statusEmo.fire} Error fetching firmware update info for Kunai GC`)
    );
  }
};

export default checkKunaiGc;
