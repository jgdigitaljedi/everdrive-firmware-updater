import chalk from 'chalk';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { getFenrirFw } from './deviceLogic/checkFenrirFw.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkFenrir = async () => {
  try {
    const newFw = await getFenrirFw();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('fenrir', 'odes', newFw.version);
      const downloaded = await downloadFw(
        newFw,
        `../downloads/ODEs/fenrir_${newFw.version}.duo`,
        `${statusEmo.fire} Error downloading firmware for Fenrir`
      );
      if (downloaded) {
        console.log(chalk.magenta(`${statusEmo.game} Downloaded firmware for your Fenrir!\n`));
      }
    }
  } catch (error) {
    console.log(chalk.red.bold(`${statusEmo.fire} Error fetching update info for Fenrir`));
  }
};

export default checkFenrir;
