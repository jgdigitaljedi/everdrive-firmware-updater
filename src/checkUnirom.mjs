import genericGhDevices from '../util/constants/genericGhDevices.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkUnirom = async () => {
  const device = genericGhDevices.unirom;
  await genericGhUpdate(device);
};

export default checkUnirom;
