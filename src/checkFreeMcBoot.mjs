import chalk from 'chalk';
import { getFreeMcBootUpdates } from './deviceLogic/checkFreeMcBootVersion.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkFreeMcBoot = async () => {
  try {
    const newFw = await getFreeMcBootUpdates();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('freemcboot', 'software', newFw.version);
      await downloadFw(
        newFw,
        `../downloads/software/freeMcBoot_${newFw.version}.zip`,
        `${statusEmo.fire} Error downloading latest version of Free McBoot`
      );
      console.log(chalk.magenta(`${statusEmo.game} Downloaded latest version of Free McBoot!\n`));
    }
  } catch (error) {
    console.log(chalk.red.bold(`${statusEmo.fire} Error fetching update info for Free McBoot`));
  }
};

export default checkFreeMcBoot;
