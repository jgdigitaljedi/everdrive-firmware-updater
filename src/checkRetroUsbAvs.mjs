import chalk from 'chalk';
import { getRetroUsbAvsFw } from './deviceLogic/checkRetroUsbAvsFw.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkRetroUsbAvs = async () => {
  try {
    const newFw = await getRetroUsbAvsFw();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('retroUsbAvs', 'devices', newFw.version);
      await downloadFw(
        newFw,
        `../downloads/devices/retroUsbAvs_${newFw.version}.bin`,
        `${statusEmo.fire} Error downloading latest version of RetroUSB AVS firmware`
      );
      console.log(
        chalk.magenta(`${statusEmo.game} Downloaded latest version of RetroUSB AVS firmware!\n`)
      );
    }
  } catch (error) {
    console.log(
      chalk.red.bold(`${statusEmo.fire} Error fetching update info for RetroUSB AVS firmware`)
    );
  }
};

export default checkRetroUsbAvs;
