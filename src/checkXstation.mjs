import genericGhDevices from '../util/constants/genericGhDevices.mjs';
import { genericGhUpdate } from '../util/genericGhUpdate.mjs';

const checkXstation = async () => {
  const device = genericGhDevices.xstation;
  await genericGhUpdate(device);
};

export default checkXstation;
