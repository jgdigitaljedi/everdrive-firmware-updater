import chalk from 'chalk';
import { getGcloaderFw } from './deviceLogic/checkGcloaderFw.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkGcloaderFw = async () => {
  try {
    const newFw = await getGcloaderFw();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('gcloader', 'odes', newFw.version);
      const downloaded = await downloadFw(
        newFw,
        `../downloads/ODEs/GCLoader_${newFw.version}.zip`,
        `${statusEmo.fire} Error downloading firmware for GCLoader`
      );
      if (downloaded) {
        console.log(chalk.magenta(`${statusEmo.game} Downloaded firmware for your GCLoader!\n`));
      }
    }
  } catch (error) {
    console.log(chalk.red.bold(`${statusEmo.fire} Error fetching update info for GCLoader`));
  }
};

export default checkGcloaderFw;
