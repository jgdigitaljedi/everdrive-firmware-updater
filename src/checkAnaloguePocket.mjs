import chalk from 'chalk';
import { getAnaloguePocketFw } from './deviceLogic/checkAnaloguePocketFw.mjs';
import { downloadFw } from '../util/downloadNewFile.mjs';
import { writeUpdatedConfig } from '../util/writeUpdatedConfig.mjs';
import { statusEmo } from '../util/constants/statusEmojies.mjs';

const checkAnaloguePocket = async () => {
  try {
    const newFw = await getAnaloguePocketFw();
    if (newFw && !newFw.error) {
      await writeUpdatedConfig('analoguePocket', 'devices', newFw.version);
      await downloadFw(
        newFw,
        `../downloads/devices/analoguePocket_${newFw.version}.bin`,
        `${statusEmo.fire} Error downloading firmware for Analogue Pocket`
      );
      console.log(
        chalk.magenta(`${statusEmo.game} Downloaded firmware for you Analogue Pocket!\n`)
      );
    }
  } catch (error) {
    console.log(chalk.red.bold(`${statusEmo.fire} Error fetching update info for Analogue Pocket`));
  }
};

export default checkAnaloguePocket;
