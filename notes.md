# Notes

## Flashcarts

### Problems

- BennVenn Atari Lynx "El Cheapo" firmware site has no version
- AtariGamer Atari Lynx "El Cheapo" firmware has paywall
- NES PowerPak discontinued so no more firmware updates
- NeoSD does not allow distribution of fw; must register for an account to get it
- Super SD system 3 (PC Engine) does not allow distribution of fw; must register for an account to get it
- Jaguar GameDrive fw not found as the website says to insert your GD and follow the link on your TV
- IntelliVision Hive Multi-cart fw nowhere to be found
- Colecovision AtariMax Ultimate SD requires you to email creator for fw
- Atari 5200 AtariMax Ultimate SD requires you to email creator for fw
- Colecovision 192-dip switch cart site is in another language
- R4 cards site have dead links

## ODEs

### Problems

- PSIO does not allow distribution of firmware; must email them for fw
- MODE for DC and PS is TerraOnion product who requires a login to get fw
- GDEMU fw is hosted on a file sharing service and there's no real way to get it programatically

## Devices

### Links

- Abernic RG35XX - https://win.anbernic.com/download/270.html

### Problems

- Some Abernic handhelds have SPA page with tabs

## Code

- see about further generalizing GH calls to lessen code for each device using GH
  - gbinterceptor is not generic logic
  - harmonycart is not generic logic
  - opl is not generic logic
- maybe make everdrives FW downloads go to EverDrive folder for consistency with config?
- what else can I add
  - most emulators are self updating or at least have an option from within
